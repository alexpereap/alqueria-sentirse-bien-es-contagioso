<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Base extends CI_Controller {
	

	public function index(){
		echo 'base controller';
	}

	//utlity to json all responses
	protected function _outputAsJSON($obj){

		$this->output->set_content_type('application/json');
		if(isset($_REQUEST['callback'])){
			$this->output->set_output($_REQUEST['callback'].'('.json_encode ($obj).')');
		}else{
			$this->output->set_output(json_encode ($obj));
		}
	}

	
	/**
     *
     */
    protected function _debug( /**/ ) {
       $var=print_r(func_get_args(),true);
		echo "<pre style='background-color:#dddd00; border: dashed thin #000000; font-size:12px'> $var</pre>";
    }
	
}

