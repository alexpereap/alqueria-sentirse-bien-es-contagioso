<?php

require __DIR__ . '/../libraries/facebook-php-sdk-v4-4.0-dev/autoload.php';

use Facebook\FacebookSession;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookRequest;
use Facebook\FacebookResponse;
use Facebook\FacebookSDKException;
use Facebook\FacebookRequestException;
use Facebook\FacebookAuthorizationException;
use Facebook\GraphObject;

session_start();

include(APPPATH . '/controllers/base.php');

class Custom_story_test extends Base {

   // private $login_permissions = array('publish_actions', 'user_friends', 'user_photos','user_posts');
   private $login_permissions = array('publish_actions','user_friends ');
   private $app_id = '400928476768256';
   private $app_secret = 'e98c8754bf2ddf005a2a9380644ea751';

   public function __construct()
   {

      parent::__construct();
      $this->load->library("session");
      $this->load->helper("url");
      $this->load->library("image_lib");

      $this->load->model("main_model","model");
      $this->load->model("model_files");

  }

  public function index(){

      FacebookSession::setDefaultApplication( $this->app_id ,  $this->app_secret );

      // login helper with redirect_uri
      $helper = new FacebookRedirectLoginHelper( base_url('custom_story_test') );

      try {
         $session = $helper->getSessionFromRedirect();
       } catch( FacebookRequestException $ex ) {
                 // When Facebook returns an error
          /*$this->session->set_flashdata('post_result', 'No hemos podido públicar tu imagen, por favor intentalo de nuevo.');
          redirect( base_url('site/my_challenges') );*/
          var_dump( $ex );
       } catch( Exception $ex ) {
                 // When validation fails or other local issues
          /*$this->session->set_flashdata('post_result', 'No hemos podido públicar tu imagen, por favor intentalo de nuevo.');
          redirect( base_url('site/my_challenges') );*/
          var_dump( $ex );
       }

      // see if we have a session
      if ( isset( $session ) ) {

         try{

            $accessToken = $session->getAccessToken();

            /*$response = $facebook->api(
              'me/deslactosada:contagiar',
              'POST',
              array(
                'bienestar' => '"http://samples.ogp.me/401002523427518"'
              )
            );*/
            // handle the response

            // graph api request for user data
            $request = new FacebookRequest( $session, 'GET', '/me' );
            $response = $request->execute();

            // get response
            $graphObject = $response->getGraphObject();

            $fb_user_id = $graphObject->getProperty( 'id' );

            $user_friends = $this->_getUserFriendList( $fb_user_id, $accessToken );

            /*var_dump( $user_friends );
            die();*/


            $ed_id = "AaJae-rg9Ft0Pwin2J-MvW-QFi_vy80leLkteMOGd1Pm8tJ2J6OMuofl_bEqr16L4GMz6jAedaAES0Jw8XOSwlbvySuF7GQFFqHhaVEmXSeYsQ";
            $al_id = "AaKRDKgB5ApkrlhDnI1VNspxBZ8KaxL8uOrQq8Nt1wsAPja6l9ZZvl26KG2POEC__zwy7NAdJUqwrwP0UHeXWJnDabIy8i3BuPU85WQ711PZ0w";


            $post_req_data = array(
               'bienestar' => "http://www.sentirsebienescontagioso.com/custom_story_test/action_type/positivismo",
               // 'tags'      => "$ed_id, $al_id"
            );


            $request = ( new FacebookRequest( $session, 'POST', '/me/deslactosada:contagiar', $post_req_data ))->execute();
            // Get response as an array, returns ID of post
            $response = $request->getGraphObject()->asArray();

            var_dump( $response );
         } catch( FacebookRequestException $ex ) {
            echo "ex posting";
            var_dump( $ex );

         } catch( Exception $ex ) {
            echo "ex posting";
            // var_dump( $ex );
         }

      } else {
         echo  '<a href="' . $helper->getLoginUrl( $this->login_permissions ) . '" >Action</a>';
      }
  }

  private function _getUserFriendList($user_id, $token){


      /**
       * You should be aware the even after a review and permission to use taggable_friends, the IDs you get back can only be used to tag friends - you can't use those IDs to get any other information about a user
       * @see http://stackoverflow.com/questions/23507885/retrieve-full-list-of-friends-using-facebook-api
      */
      $url = 'https://graph.facebook.com/v2.3/' . $user_id . '/taggable_friends?access_token=' . $token . '&pretty=1&limit=5000';
      $grap_object = json_decode(file_get_contents( $url ));

      // parse data to user firends

      $user_friends = array();

      foreach( $grap_object->data as $friend ){

          $user_friends[] = array(
              'label'   => $friend->name,
              'id'      => $friend->id,
              'picture' => $friend->picture->data->url
          );
      }

      return $user_friends;
    }

    public function action_type( $title ){

      $this->load->model('main_model');

        $action_type = $this->main_model->getActionTypeByTitle( $title );

        if( $action_type === false ){

          show_404();
          return;
        }

        $data = array(
          'action_type' => $action_type
        );

        $this->load->view('action_type_test', $data);

    }

}