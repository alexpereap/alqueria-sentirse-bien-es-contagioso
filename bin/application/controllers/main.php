<?php

require __DIR__ . '/../libraries/facebook-php-sdk-v4-4.0-dev/autoload.php';



use Facebook\FacebookSession;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookRequest;
use Facebook\FacebookResponse;
use Facebook\FacebookSDKException;
use Facebook\FacebookRequestException;
use Facebook\FacebookAuthorizationException;
use Facebook\GraphObject;

session_start();

include(APPPATH . '/controllers/base.php');

class Main extends Base {

   // private $login_permissions = array('publish_actions', 'user_friends', 'user_photos','user_posts');
   private $login_permissions = array();
   private $app_id = '400928476768256';
   private $app_secret = 'e98c8754bf2ddf005a2a9380644ea751';   

   public function __construct()
   {

      parent::__construct();
      $this->load->library("session");
      $this->load->helper("url");
      $this->load->library("image_lib");

      $this->load->model("main_model","model");
      $this->load->model("model_files");

  }

  public function countPosts()
  {
    echo $this->model->countPosts();
  }

  public function index2()
  {
      $content = $this->model->getContent();

      $this->reset_session();
      $post_id = parse_str(substr(strrchr($_SERVER['REQUEST_URI'], "?"), 1), $_GET);
      
      $posted = isset($_GET['post_id']);
      //var_dump($posted);
      $user_count = 0;
      $winner = "0";
      $next_winner_pos = 0;

      if ($posted)
      {
        $this->model->addPost(array('message' => "shared on fb", 'image_path' => 'image','social_network' => 'fb','creation_date' => date("Y-m-d H:i:s")));
        $user_count = $this->model->countPosts();
        $next_winner_pos = $this->model->getNextWinnerPosition();
        if ($user_count >= $next_winner_pos )
        {
            $winner = "1";
        }
      }



      FacebookSession::setDefaultApplication( $this->app_id ,  $this->app_secret );

      // login helper with redirect_uri
      $helper = new FacebookRedirectLoginHelper( base_url('main/index') );

      try {
         $session = $helper->getSessionFromRedirect();
     } catch( FacebookRequestException $ex ) {
         echo ("facebook error:: " . $ex->getMessage());
        // When Facebook returns an error
     } catch( Exception $ex ) {
         echo ("other " . $ex->getMessage());
        // When validation fails or other local issues
     }

       // see if we have a session
     if ( isset( $session ) ) {

         try{

            // graph api request for user data

            $fbrequest = new FacebookRequest( $session, 'GET', '/me?fields=id,name,taggable_friends.limit(10000)');

            $response = $fbrequest->execute();

            $graphObject = $response->getGraphObject();
            // var_dump($response);


            $fb_id = $graphObject->getProperty( 'id' );
            $friends = $this->_get_friends($graphObject);

            //var_dump($friends);

            $this->session->set_userdata("fbid", $fb_id);

            $data = array(
               'fb_session' => true,
               'posted' => $posted,
               'fbid' => $fb_id,
               'friends' => $friends,
               'login_url' => $this->_get_fb_login_url(),
               'winner' => $winner,
               'position' => $next_winner_pos,
               'user_count' => $user_count,
               'content' => $content
               );
            //var_dump($data);
           $this->load->view('header',$data);
           $this->load->view('index_new');
           $this->load->view('footer');


        }  catch( FacebookRequestException $ex ) {
            // When Facebook returns an error
            echo "fb<br>";
            echo "<pre>";
            print_r($ex);
            echo "</pre>";
        } catch( Exception $ex ) {
            // When validation fails or other local issues
            echo "local</br>";
            var_dump($ex);
        }


        } else { // not isset session
        // show login url

           $data = array(
              'fb_session' => false,
              'posted' =>false,
              'login_url' => $this->_get_fb_login_url(),
               'winner' => $winner,
               'position' => $next_winner_pos,
               'user_count' => $user_count,
               'registered' => true,
               'content' => $content
              );
            // var_dump($data);
           $this->load->view('header',$data);
           $this->load->view('index_new');
           $this->load->view('footer');

       }
  }

  public function index()
  {
    $this->reset_session();
      $post_id = parse_str(substr(strrchr($_SERVER['REQUEST_URI'], "?"), 1), $_GET);
      
      $posted = isset($_GET['post_id']);
      //var_dump($posted);
      $user_count = 0;
      $winner = "0";
      $next_winner_pos = 0;

      if ($posted)
      {
        $this->model->addPost(array('message' => "shared on fb", 'image_path' => 'image','social_network' => 'fb','creation_date' => date("Y-m-d H:i:s")));
        $user_count = $this->model->countPosts();
        $next_winner_pos = $this->model->getNextWinnerPosition();
        if ($user_count >= $next_winner_pos )
        {
            $winner = "1";
        }
      }



      FacebookSession::setDefaultApplication( $this->app_id ,  $this->app_secret );

      // login helper with redirect_uri
      $helper = new FacebookRedirectLoginHelper( base_url('main/index') );

      try {
         $session = $helper->getSessionFromRedirect();
     } catch( FacebookRequestException $ex ) {
         echo ("facebook error:: " . $ex->getMessage());
        // When Facebook returns an error
     } catch( Exception $ex ) {
         echo ("other " . $ex->getMessage());
        // When validation fails or other local issues
     }

       // see if we have a session
     if ( isset( $session ) ) {

         try{

            // graph api request for user data

            $fbrequest = new FacebookRequest( $session, 'GET', '/me?fields=id,name,taggable_friends.limit(10000)');

            $response = $fbrequest->execute();

            $graphObject = $response->getGraphObject();
            // var_dump($response);


            $fb_id = $graphObject->getProperty( 'id' );
            $friends = $this->_get_friends($graphObject);

            //var_dump($friends);

            $this->session->set_userdata("fbid", $fb_id);

            $data = array(
               'fb_session' => true,
               'posted' => $posted,
               'fbid' => $fb_id,
               'friends' => $friends,
               'login_url' => $this->_get_fb_login_url(),
               'winner' => $winner,
               'position' => $next_winner_pos,
               'user_count' => $user_count
               );
            $data['content'] =  $content = $this->model->getContent();
           // var_dump($data);
           $this->load->view('header',$data);
           $this->load->view('index');
           $this->load->view('footer');


        }  catch( FacebookRequestException $ex ) {
            // When Facebook returns an error
            echo "fb<br>";
            echo "<pre>";
            print_r($ex);
            echo "</pre>";
        } catch( Exception $ex ) {
            // When validation fails or other local issues
            echo "local</br>";
            var_dump($ex);
        }


        } else { // not isset session
        // show login url

           $data = array(
              'fb_session' => false,
              'posted' =>false,
              'login_url' => $this->_get_fb_login_url(),
               'winner' => $winner,
               'position' => $next_winner_pos,
               'user_count' => $user_count
              );

          $data['content'] =  $content = $this->model->getContent();

            // var_dump($data);
           $this->load->view('header',$data);
           $this->load->view('index');
           $this->load->view('footer');

       }

   }

   private function _get_friends($graphObj )
   {
      $friends = $graphObj->getProperty('taggable_friends');  
      $result = array();

      if ($friends)
      {
         $friends = $friends->asArray();
         foreach ($friends['data'] as $friend) {
            array_push($result, array("name"=>$friend->name, "pic"=>$friend->picture->data->url));
        }

    }
    return $result;

}
public function postActive()
{
  $content_id = $this->session->userdata('content_id');
  $item = $this->model->getContentById($content_id);
  var_dump($item);
}

public function twitter()
{

    $data = array();
    $data['session_id'] = $this->session->flashdata('session_id');
    $content_id = $this->session->userdata('content_id');

    $item = $this->model->getContentById($content_id);

    $data['content_id'] = ($item->type == "image") ? "i" : "a";


    $data['msg'] = "Hoy me contagié de bienestar, contágiate tú también. http://bit.ly/1SnNxhS";
    $data['img'] = $item->share_img;
    $data['url'] = $item->url;
    $data['user_count'] = 0;
    $data['winner'] = "0";
    $data['content'] =  $content = $this->model->getContent();


    $this->load->view('header',$data);
    $this->load->view('twitter');
    $this->load->view('footer');
}

public function shareVideo($content_id)
{
  $item = $this->model->getContentById($content_id);
  if ($item->type == "audio")
  {
      $data =  array('final_url' => $item->url);
      $this->load->view("video_redirect", $data);
  }
  else
  {
    $new_loaction = base_url();
    header("Location:$new_loaction");
  }

}

public function formFacebook()
  {
    
    $image_url_str = $_POST['image_url'];
    
    if(strlen($image_url_str))
    { 
        /*$foto_perfil = fopen($_POST['image_url'], 'r' );
        $file_foto  = "pic_" . time() . ".jpg";
        $dir_foto   = './uploads/' . $file_foto;
        var_dump($dir_foto);
        die();*/
        $source_file = 'http://www.sentirsebienescontagioso.com/img/';
       // $source_file = 'img/';
        $dir_foto = $source_file.$image_url_str;

        // file_put_contents($dir_foto, $foto_perfil );
        // $this->session->set_userdata('post_fb',array('msg' => $_POST['comment_user'] ,'url' => base_url($dir_foto) ));
        $this->session->set_userdata('post_fb', array('msg' => $_POST['comment_user'],
            'url' => $dir_foto ));
        
        $data=$this->session->userdata('post_fb');

        $this->model->addPost(array('message' => $_POST['comment_user'], 'image_path' =>base_url($dir_foto), 'social_network' =>'fb','creation_date' => date("Y-m-d H:i:s")));

        $this->session->set_flashdata('success', 'Tu publicacion se ha realizado exitosamente.');
        redirect($this->_get_fb_main_post_onfb());
    } else {
                  // redirect( base_url() . 'site/set_purchase_data?error=' . $rf['error'] );
                  // krumo($rf['error']);
                  $this->session->set_flashdata('success_title', 'Ha ocurrido un error');
                  $this->session->set_flashdata('success', 'Intenta subir tu reto de nuevo haciendo click <a href="' . base_url('main/index') . '" >aquí</a>');
                  //redirect(base_url('main/index'));
              }
      //$this->session->set_userdata('post_fb',array('msg' => $_POST['msg'] ,'url' => $_POST[''] ););
      //redirect($this->_get_fb_main_post_onfb());
  }

public function post_on_facebook()
{
    FacebookSession::setDefaultApplication( $this->app_id ,  $this->app_secret );

      // login helper with redirect_uri
    $helper = new FacebookRedirectLoginHelper( base_url('main/post_on_facebook') );

    try {
       $session = $helper->getSessionFromRedirect();
   } catch( FacebookRequestException $ex ) {
       echo("facebook error:: " . $ex->getMessage());
        // When Facebook returns an error
   } catch( Exception $ex ) {
       echo("other " . $ex->getMessage());
        // When validation fails or other local issues
   }

      // see if we have a session
   if ( isset( $session ) ) {
         // echo "session";
       try{
          $msg = $this->session->userdata('post_fb');

          $post_req_data =  array(
             'message'      => $msg['msg'],
             'url'          => $msg['url']
             );
            // graph api request for user data
          $request = ( new FacebookRequest( $session, 'POST', '/me/photos', $post_req_data ))->execute();

          var_dump($request);
            // get response
          $graphObject = $request->getGraphObject();

             $user_count = $this->model->countPosts();

            //var_dump($graphObject);

          $data = array(
             'fb_session' => true,
             'posted' =>true,
             'fbid' => $this->session->userdata("fbid")
             );

           $this->load->view('header',$data);
           $this->load->view('index');
           $this->load->view('footer');

      }  catch( FacebookRequestException $ex ) {
            // When Facebook returns an error
          echo "fb<br>";
          echo "<pre>";
          print_r($ex);
          echo "</pre>";
      } catch( Exception $ex ) {
            // When validation fails or other local issues
          echo "local</br>";
          var_dump($ex);
      }


        } else { // not isset session
        // show login url
           $data = array(
              'fb_session' => false,
              'login_url' => $this->_get_fb_login_url()
              );
           $this->load->view('header',$data);
           $this->load->view('index');
           $this->load->view('footer');

       }

   }

   public function userRegistration() 
   {
    $content = $this->model->getContent();
    $winner_position = $this->model->getNextWinnerPosition();
      $data = array(
         'name' => $_POST['name'],
         'phone' => $_POST['phone'],
         'birth_date' => $_POST['birth_date_day'].'/'.$_POST['birth_date_month'].'/'.$_POST['birth_date_year'],
         'address' => $_POST['address'],
         'city' => $_POST['city'],
         'terms' => $_POST['terms'],
         'email' => $_POST['email'],
         'creation_date' => date("Y-m-d H:i:s"),
         'position' => $this->model->getNextWinnerPosition()  
         );

      //- Register
      $dataUser = $this->model->setUser($data);
      $this->model->setWinner($winner_position);

      if ($dataUser) {
         $result = array(
            'success' => true,
            'msg'=> 'Se ha registrado correctamente'
            );
     } else {
         $result = array(
            'success' => false,
            'msg'=> 'No se ha podido  registrar'
            );
     }
    $data = array(
        'posted' => false,
        'fb_session' => false,
        'registered' => true,
        'user_count' => 0,
        'content' => $content
        );
     $this->load->view('header',$data);
     $this->load->view('index');
     $this->load->view('footer');
    //return $this->_outputAsJSON($result);
 }

public function phpinfo()
{
    phpinfo();
}

public function content($id)
{
    $this->load->view('content'.$id);
}
public function page()
{
    $this->load->view('page');
}

protected function _get_fb_login_url(){
    FacebookSession::setDefaultApplication( $this->app_id ,  $this->app_secret );

        // login helper with redirect_uri
    $helper = new FacebookRedirectLoginHelper( base_url('main/index') );
        // $helper = new FacebookRedirectLoginHelper( base_url() );
    return $helper->getLoginUrl( $this->login_permissions );
}

private function _get_fb_main_post_onfb(){
    FacebookSession::setDefaultApplication( $this->app_id ,  $this->app_secret );

        // login helper with redirect_uri
    $helper = new FacebookRedirectLoginHelper( base_url('main/post_on_facebook') );
        // $helper = new FacebookRedirectLoginHelper( base_url() );
    return $helper->getLoginUrl( $this->login_permissions );
}
  /**
   * Reset session data
   * @access  private
   * @return  void
   */
  protected function reset_session()
  {
    $this->session->unset_userdata('access_token');
    $this->session->unset_userdata('access_token_secret');
    $this->session->unset_userdata('request_token');
    $this->session->unset_userdata('request_token_secret');
    $this->session->unset_userdata('twitter_user_id');
    $this->session->unset_userdata('twitter_screen_name');
  }

}
