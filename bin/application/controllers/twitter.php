<?php
/**
 * Twitter OAuth library.
 * Sample controller.
 * Requirements: enabled Session library, enabled URL helper
 * Please note that this sample controller is just an example of how you can use the library.
 */
defined('BASEPATH') OR exit('No direct script access allowed');

include(APPPATH . '/controllers/main.php');

class Twitter extends Main
{
	/**
	 * TwitterOauth class instance.
	 */
	private $connection;
	
	/**
	 * Controller constructor
	 */
	function __construct()
	{
		parent::__construct();
		$this->load->library("session");
		// Loading TwitterOauth library. Delete this line if you choose autoload method.
		$this->load->library('twitteroauth');
		// Loading twitter configuration.
		$this->config->load('twitter');
		$this->load->library("image_lib");

		$this->load->model('main_model','model');
		$this->load->model("model_files");
		
		if($this->session->userdata('access_token') && $this->session->userdata('access_token_secret'))
		{
			// If user already logged in
			$this->connection = $this->twitteroauth->create($this->config->item('twitter_consumer_token'), $this->config->item('twitter_consumer_secret'), $this->session->userdata('access_token'),  $this->session->userdata('access_token_secret'));
		}
		elseif($this->session->userdata('request_token') && $this->session->userdata('request_token_secret'))
		{
			// If user in process of authentication
			$this->connection = $this->twitteroauth->create($this->config->item('twitter_consumer_token'), $this->config->item('twitter_consumer_secret'), $this->session->userdata('request_token'), $this->session->userdata('request_token_secret'));
		}
		else
		{
			// Unknown user
			$this->connection = $this->twitteroauth->create($this->config->item('twitter_consumer_token'), $this->config->item('twitter_consumer_secret'));
		}
	}
	
	/**
	 * Here comes authentication process begin.
	 * @access	public
	 * @return	void
	 */
	public function auth()
	{
		/*if($this->session->userdata('access_token') && $this->session->userdata('access_token_secret'))
		{
			// User is already authenticated. Add your user notification code here.
			redirect(base_url('/twitter/callback'));
		}
		else
		{*/
			// Making a request for request_token
			$request_token = $this->connection->getRequestToken(base_url('/twitter/callback'));

			$this->session->set_userdata('request_token', $request_token['oauth_token']);
			$this->session->set_userdata('request_token_secret', $request_token['oauth_token_secret']);
			
			if($this->connection->http_code == 200)
			{
				$url = $this->connection->getAuthorizeURL($request_token);
				redirect($url);
			}
			else
			{
				// An error occured. Make sure to put your error notification code here.
				redirect(base_url('main/index'));
			}
		//}
	}
	
	/**
	 * Callback function, landing page for twitter.
	 * @access	public
	 * @return	void
	 */
	public function callback()
	{
		if($this->input->get('oauth_token') && $this->session->userdata('request_token') !== $this->input->get('oauth_token'))
		{
			$this->reset_session();
			redirect(base_url('/twitter/auth'));
		}
		else
		{
			$access_token = $this->connection->getAccessToken($this->input->get('oauth_verifier'));
		
			if ($this->connection->http_code == 200)
			{
				$this->session->set_userdata('access_token', $access_token['oauth_token']);
				$this->session->set_userdata('access_token_secret', $access_token['oauth_token_secret']);
				$this->session->set_userdata('twitter_user_id', $access_token['user_id']);
				$this->session->set_userdata('twitter_screen_name', $access_token['screen_name']);


				$this->session->unset_userdata('request_token');
				$this->session->unset_userdata('request_token_secret');
				
				/**
				* Get user profile pic
				*/
				//$userProfilePic = $this->_get_user_profile_pic(array('user_id' => $access_token['user_id'], 'screen_name' => $access_token['screen_name']));
				//$this->session->set_userdata('twitter_profile_pic', $userProfilePic);

				//$this->getTwetts();
				$this->session->set_flashdata('twitter_user_id', $this->session->userdata('twitter_user_id'));
				$this->session->set_flashdata('session_id', $this->session->userdata('session_id'));
				redirect(base_url('main/twitter'));
			}
			else
			{
				// An error occured. Add your notification code here.
				redirect(base_url('/'));
			}
		}
	}

	public function twitt($content_id)
	{
		$this->session->set_userdata('content_id', $content_id);
		$this->auth();
	}

	private function _get_user_profile_pic($inputs)
	{
		$result = $this->connection->get('users/show', $inputs);
		$final_path = str_replace("_normal", "", $result->profile_image_url);
		return $final_path;
	}
	
	public function post($in_reply_to)
	{
		$message = $this->input->post('message');
		if(!$message || mb_strlen($message) > 140 || mb_strlen($message) < 1)
		{
			// Restrictions error. Notification here.
			redirect(base_url('/'));
		}
		else
		{
			if($this->session->userdata('access_token') && $this->session->userdata('access_token_secret'))
			{
				$content = $this->connection->get('account/verify_credentials');
				if(isset($content->errors))
				{
					// Most probably, authentication problems. Begin authentication process again.
					$this->reset_session();
					redirect(base_url('/twitter/auth'));
				}
				else
				{
					$data = array(
						'status' => $message,
						'in_reply_to_status_id' => $in_reply_to
					);
					$result = $this->connection->post('statuses/update', $data);

					if(!isset($result->errors))
					{
						// Everything is OK
						redirect(base_url('/'));
					}
					else
					{
						// Error, message hasn't been published
						redirect(base_url('/'));
					}
				}
			}
			else
			{
				// User is not authenticated.
				redirect(base_url('/twitter/auth'));
			}
		}
	}
	

	public function shareResult()
	{
	 
	$login_url = $this->_get_fb_login_url();
	$content = $this->model->getContent();

    $image_url_str = $_POST['image_url'];
    
    if(strlen($image_url_str))
    {
         $user_count = 0;
      	 $winner = "0";

	  
        $source_file = '/home/alqueria_usr/sentirsebienescontagioso.com/img/post/';
        $dir_foto = $source_file.$image_url_str;
    
        $this->session->set_userdata('post_fb',array('msg' => $_POST['comment_user'] ,'url' => base_url($dir_foto),'creation_date' => date("Y-m-d H:i:s") ));
        $this->model->addPost(array('message' => $_POST['comment_user'], 'image_path' =>base_url($dir_foto),'social_network' => 'tw','creation_date' => date("Y-m-d H:i:s")));
  
        $this->session->set_flashdata('success', 'Tu publicacion se ha realizado exitosamente.');
        $media = "@$dir_foto;type=image/png;filename=default.png";
        $data = array(
					'media[]' => $media,
					'status' => $_POST['comment_user'] ,
					"url" => "http://www.sentirsebienescontagioso.com/"
				);
        $result = $this->connection->post('statuses/update_with_media', $data, true);

        $user_count = $this->model->countPosts();
        $next_winner_pos = $this->model->getNextWinnerPosition();
        if ($user_count >= $next_winner_pos )
        {
            $winner = "1";
        }

		 $view_data = array(
        'tw_session' => true,
        'posted' => true,
        'winner' => $winner,
        'login_url' =>$login_url,
        'position' =>$next_winner_pos,
        'user_count' =>$user_count,
        'content' => $content
    	);

		$this->reset_session();
		// var_dump($view_data);
		$this->load->view('header',$view_data);
		$this->load->view('index');
		$this->load->view('footer');

    } else {
                  // redirect( base_url() . 'site/set_purchase_data?error=' . $rf['error'] );
                  // krumo($rf['error']);
			$this->reset_session();
    		$this->session->set_flashdata('success_title', 'Ha ocurrido un error');
    		$this->session->set_flashdata('success', 'Intenta subir tu reto de nuevo haciendo click <a href="' . base_url('main/index') . '" >aquí</a>');
                  //redirect(base_url('main/index'));
    	}
      //$this->session->set_userdata('post_fb',array('msg' => $_POST['msg'] ,'url' => $_POST[''] ););
      //redirect($this->_get_fb_main_post_onfb());

    }
    public function shareTweet()
	{
	 
	$login_url = $this->_get_fb_login_url();
	$content = $this->model->getContent();

         $user_count = 0;
      	 $winner = "0";

       // $this->session->set_userdata('post_fb',array('msg' => $_POST['comment_user'] ,'url' => base_url($dir_foto),'creation_date' => date("Y-m-d H:i:s") ));
        $this->model->addPost(array('message' => $_POST['comment_user'], 'image_path' =>'imagetwAudio','social_network' => 'tw','creation_date' => date("Y-m-d H:i:s")));
  
        $this->session->set_flashdata('success', 'Tu publicacion se ha realizado exitosamente.');
        //$media = "@$dir_foto;type=image/png;filename=default.png";
        $data = array(
					//'media[]' => $media,
					'status' => $_POST['comment_user'] ,
					"url" => "http://www.sentirsebienescontagioso.com/"
				);
        $result = $this->connection->post('statuses/update', $data, true);

        $user_count = $this->model->countPosts();
        $next_winner_pos = $this->model->getNextWinnerPosition();
        if ($user_count >= $next_winner_pos )
        {
            $winner = "1";
        }

		 $view_data = array(
        'tw_session' => true,
        'posted' => true,
        'winner' => $winner,
        'login_url' =>$login_url,
        'position' =>$next_winner_pos,
        'user_count' =>$user_count,
        'content' => $content
    	);

		$this->reset_session();
		// var_dump($view_data);
		$this->load->view('header',$view_data);
		$this->load->view('index');
		$this->load->view('footer');

    }
	  
	public function composeImageToShare($source)
	{
    /**
    * $source = './uploads/test1.jpg';
    */

    $config = array();
    $full_path = str_replace("/./", "/", base_url($source));
    $src = base_url('timthumb.php?src=' . $full_path . '&w=800&h=800&zc=1&f=2|4,-50');
    $thumb = fopen($src, 'r' );
    file_put_contents($source, $thumb );


    $config['wm_type'] = 'overlay';

    $imageNo=$this->session->userdata('img');
    /*var_dump($imageNo);
    die();*/

    $config['wm_overlay_path'] = $imageNo;
    $config['wm_opacity'] = 100;
    $config['wm_hor_offset'] = 0;
    $config['wm_vrt_offset'] = 0;
    $config['source_image'] = $source;      
    $config['wm_x_transp'] = 40;
    $config['wm_y_transp'] = 40;

    $this->image_lib->clear();
    $this->image_lib->initialize($config); 

    if (!$this->image_lib->watermark())
    {
    	echo $this->image_lib->display_errors();
    }
    else
    {
    	return true;
    }
}
	private function _no_count($tweets, $needle)
	{
		$no_count = 0;
	    $needles = explode(',', $needle);

		foreach ($tweets as $tw) {
			for ($i = 0; $i < count($needles); $i++)
			      {
			        $needle = $needles[$i];
			        $haystack = strtolower($tw->text);
			        $pos = strpos($haystack, $needle);
			        if ($pos !== false)
			        {
			          $no_count++;
			        }
			      }
          }

		return $no_count;
	}
	
}

/* End of file twitter.php */
/* Location: ./application/controllers/twitter.php */