<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class main_model extends CI_Model{

	public function __construct()
	{
	    parent::__construct();
	   	$this->load->database();
	}

	public function addPost($inputs)
	{
		$this->db->insert('tbl_posts', $inputs);
	}

	public function countPosts()
	{
		return $this->db->count_all_results('tbl_posts');
	}
	public function getNextWinnerPosition()
	{
		$query = $this->db->where("has_winner", "N");
		$query = $this->db->order_by("position", "asc");
		$query = $this->db->get("tbl_winner_position");

		$result = $query->result();
		if (empty($result))
		{
			return 100000000;
		}
		else
		{
			$record = $query->first_row();
			return intval($record->position);
		}
	}

	public function getContent()
	{
		$status = 'active';
		$this->db->where('status', $status);
		$this->db->order_by('id', "random");
		$query = $this->db->get('tbl_content');
		return $query->result();
	}

	public function getContentById($id)
	{
		$this->db->where('id', $id);
		$query = $this->db->get('tbl_content');
		return $query->first_row();
	}

	public function gallery()
	{
		$this->db->select('image_path');
		$this->db->order_by('creation_date', "desc");
		$query = $this->db->get('tbl_posts');

		return $query->result();
	}

	public function setUser($inputs)
	{
		$query = $this->db->insert('tbl_user', $inputs);
		if($query) {
			return $this->db->insert_id();
		} else {
			return false;
		}
	}

	public function setWinner($position)
	{
		$data = array('has_winner' => "Y");

		$this->db->where("position",$position);
		$this->db->update("tbl_winner_position", $data);

	}

	public function getActionTypeByTitle( $title ){

		$q = $this->db->get_where('tbl_actions_types', array(
			'title' => $title
		));

		return count( $q->row() ) > 0 ? $q->row() : false;
	}
}
