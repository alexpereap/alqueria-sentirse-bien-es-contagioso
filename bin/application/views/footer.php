<!--FOOTER COPYRIGHTS AND MODAL-->
    <!--MODAL MESSAGE: TERMS & CONDITIONS-->
      <article class="remodal messages-mdl terms-conditions-mdl" data-remodal-id="terms-conditions-message-footer" data-remodal-options="hashTracking: true">
        <header class="header-modal-deslactosada">
          <hgroup><img src="img/header-title.png" /></hgroup>
        </header>
          <h2 class="mdl-subtitles">Términos y condiciones</h2>
          <hr class="terms-hr" />
            <div class="terms-box contentToScroll">
              <h5>¿Quiénes pueden participar?</h5>
              Todas aquellas personas que deseen contagiar a sus amigos de lo bien que se siente mejor aún si lo ha expresado en sus redes sociales facebook y/o Twitter.<br/><br/>
              <h5>¿Cómo participar?</h5>
              Puede hacerlo ingresando directamente al sitio <br/>
              <a href="#">www.sentirsebienescontagioso.com</a> o por la invitación que reciba de la Vaca Madrina en alguno de sus posts positivos dentro de facebook o Twitter. Una vez dentro del sitio sólo debe seguir los siguientes pasos:<br/><br/>
              <h6>1.</h6> Seleccione el mensaje que desea compartir a uno o más amigos. Puede hacerlo por medio de dos alternativas que le ofrece el sitio: imagen o un audio.<br/><br/>
              <h6>2.</h6> Conéctese a cualquiera de las dos redes sociales disponibles (facebook o Twitter) con el usuario y contraseña que usa para acceder a ellas y posteriormente seleccione los amigos a quienes desea compartir el mensaje (en el caso de facebook) o escriba publique el mensaje tagueando a sus amigos en el caso de Twitter (Ej. @sebastian23). Realice la publicación haciendo clic en el botón "Enviar mensaje". Automáticamente quedará publicado en el timeline del usuario un mensaje alusivo al elemento compartido.<br/><br/>
              Por cada amigo al que le comparta un mensaje recibirá un punto. En el caso de Twitterr recibira punto por cada mensaje posteado. Quien acumule el mayor número de puntos recibirá en el domicilio que haya registrado un reconocimiento por parte de Alquería. Sólo podrá compartir dos veces por semana a unmismo amigo. Ser elegirá un usuario reconocido cada semana desde el XX de XXXXX hasta el XX de XXXXX el cual será informado en la fanpage de la actividad.<br/><br/>
              <h6>3.</h6> Sólo cuando se lleve a cabo la primera publicación de mensaje, el sistema le solicitará que ingrese los datos de ubicación para poder hacer envío del reconocimiento en caso que el usuario se haga acreedor a ello.<br/><br/>
              El organizador de la actividad se reserva el derecho se asignación de reconociemintos a usuarios que lleven a cabo actividades fraudulentas o que al menos se sugiera sospecha de ello. La presente actividad busca motivar a los usuarios a contagiar sus sentimientos postivos más allá del mérito competitivo que eso suponga. Por tal motivo el reconomiciento -como su nombre lo indica- es una iniciativa del organizador para agradecer la participación de los usuarios y el cálculo de los puntos acumulados para otorgarlo se hace conforme a lo que el sistema va almacenando de manera privada.
              <br/>
            </div>
          <div class="ctas-mdl-wrap">
            <a href="#" data-remodal-action="cancel" class="cta-btn-mdl-msg btn-red remodal-cancel">Volver</a>
          </div><!--/.ctas-mdl-wrap-->
      </article><!--/.remodal.success-register-mdl-->

       <!--MODAL MESSAGE: TERMS & CONDITIONS FORM-->
      <article class="remodal messages-mdl terms-conditions-mdl returnFormClose"  data-remodal-id="terms-conditions-message" data-remodal-options="hashTracking: true">
        <header class="header-modal-deslactosada">
          <hgroup><img src="img/header-title.png" /></hgroup>
        </header>
          <h2 class="mdl-subtitles">Términos y condiciones</h2>
          <hr class="terms-hr" />
            <div class="terms-box contentToScroll">
              <h5>¿Quiénes pueden participar?</h5>
              Todas aquellas personas que deseen contagiar a sus amigos de lo bien que se siente mejor aún si lo ha expresado en sus redes sociales facebook y/o Twitter.<br/><br/>
              <h5>¿Cómo participar?</h5>
              Puede hacerlo ingresando directamente al sitio <br/>
              <a href="#">www.sentirsebienescontagioso.com</a> o por la invitación que reciba de la Vaca Madrina en alguno de sus posts positivos dentro de facebook o Twitter. Una vez dentro del sitio sólo debe seguir los siguientes pasos:<br/><br/>
              <h6>1.</h6> Seleccione el mensaje que desea compartir a uno o más amigos. Puede hacerlo por medio de dos alternativas que le ofrece el sitio: imagen o un audio.<br/><br/>
              <h6>2.</h6> Conéctese a cualquiera de las dos redes sociales disponibles (facebook o Twitter) con el usuario y contraseña que usa para acceder a ellas y posteriormente seleccione los amigos a quienes desea compartir el mensaje (en el caso de facebook) o escriba publique el mensaje tagueando a sus amigos en el caso de Twitter (Ej. @sebastian23). Realice la publicación haciendo clic en el botón "Enviar mensaje". Automáticamente quedará publicado en el timeline del usuario un mensaje alusivo al elemento compartido.<br/><br/>
              Por cada amigo al que le comparta un mensaje recibirá un punto. En el caso de Twitterr recibira punto por cada mensaje posteado. Quien acumule el mayor número de puntos recibirá en el domicilio que haya registrado un reconocimiento por parte de Alquería. Sólo podrá compartir dos veces por semana a unmismo amigo. Ser elegirá un usuario reconocido cada semana desde el XX de XXXXX hasta el XX de XXXXX el cual será informado en la fanpage de la actividad.<br/><br/>
              <h6>3.</h6> Sólo cuando se lleve a cabo la primera publicación de mensaje, el sistema le solicitará que ingrese los datos de ubicación para poder hacer envío del reconocimiento en caso que el usuario se haga acreedor a ello.<br/><br/>
              El organizador de la actividad se reserva el derecho se asignación de reconociemintos a usuarios que lleven a cabo actividades fraudulentas o que al menos se sugiera sospecha de ello. La presente actividad busca motivar a los usuarios a contagiar sus sentimientos postivos más allá del mérito competitivo que eso suponga. Por tal motivo el reconomiciento -como su nombre lo indica- es una iniciativa del organizador para agradecer la participación de los usuarios y el cálculo de los puntos acumulados para otorgarlo se hace conforme a lo que el sistema va almacenando de manera privada.
              <br/>
            </div>
          <div class="ctas-mdl-wrap">
            <a href="#" data-remodal-action="cancel" class="cta-btn-mdl-msg btn-red remodal-cancel returnFormClose">Volver</a>
          </div><!--/.ctas-mdl-wrap-->
      </article><!--/.remodal.success-register-mdl-->

    <!--FOOT-->
    <footer id="mainFoot">
      <p>Todos los derechos reservados : Alqueria S.A <span class="pipe">|</span> <a onclick="ga('send', 'event', 'Footer', 'Clic', '/Botón_Términos_Y_Condiciones');" href="<?php echo base_url(); ?>terminos_y_condiciones_sentirse_bien_es_contagioso.pdf" target="_blank">Términos y condiciones</a></p>
    </footer>
  <!--SCRIPTS-->
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
  

  <input  class="winner_flag "style="display:none;" type="text" name="is_winner" value="<?php echo $winner; ?>">
  <input  class="user_count_flag "style="display:none;" type="text" name="user_count" value="<?php echo $user_count; ?>">
  <input  class="user_registered_flag "style="display:none;" type="text" name="user_registered" value="<?php if (isset($registered) && $registered){echo 'registered';} ?>">
  <script src="<?php echo base_url(); ?>js/jquery.lazy.min.js"></script>
  <script src="<?php echo base_url(); ?>js/chromeSmoothScroll.min.js"></script>
  <script src="<?php echo base_url(); ?>js/jquery.backstretch.min.js"></script>
  <script src="<?php echo base_url(); ?>js/jquery.wookmark.js"></script>
  <script src="<?php echo base_url(); ?>js/jquery.remodal.min.js"></script>
  <script src="<?php echo base_url(); ?>js/jquery.scrollbar.min.js"></script>
  <script src="<?php echo base_url(); ?>js/jquery.simpleplayer.min.js"></script>
  <script src="<?php echo base_url(); ?>js/app.js"></script>
  <script src="<?php echo base_url(); ?>js/main.js"></script>
   <!-- Google Code for visitaron Deslactosada - Sentirse bien es contagioso- -->
    <!-- Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. For instructions on adding this tag and more information on the above requirements, read the setup guide: google.com/ads/remarketingsetup -->
    <script type="text/javascript">
    /* <![CDATA[ */
    var google_conversion_id = 982095385;
    var google_conversion_label = "WL2-CJr4rV4Qmaym1AM";
    var google_custom_params = window.google_tag_params;
    var google_remarketing_only = true;
    /* ]]> */
    </script>
    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
    </script>
    <noscript>
    <div style="display:inline;">
    <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/982095385/?value=1.00&amp;currency_code=COP&amp;label=WL2-CJr4rV4Qmaym1AM&amp;guid=ON&amp;script=0"/>
    </div>
    </noscript>
  </body>
</html>