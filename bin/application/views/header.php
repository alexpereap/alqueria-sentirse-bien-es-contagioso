<!DOCTYPE html>
<html lang='es'>
<head>
  <!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','//connect.facebook.net/en_US/fbevents.js');

fbq('init', '926731727352142');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=926731727352142&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
  <base href="<?php echo base_url() ?>">  
  <title>Leche Deslactosada - Alqueria</title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="author" content="Alqueria">
  <link href='//fonts.googleapis.com/css?family=Questrial' rel='stylesheet' type='text/css'>
  <link href='//fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,700,400' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="<?php echo base_url(); ?>css/normalize.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>css/bijou.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery.remodal.css">
  <link type="text/css"  href="<?php echo base_url(); ?>css/jquery.scrollbar.css" rel="stylesheet" media="all" />
  <link rel="stylesheet" href="<?php echo base_url(); ?>css/main.css">
  <link rel="shortcut icon" href="<?php base_url() ?>favicon.png">
  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
      <!--<link rel="icon" href="favicon.ico">-->

      <!-- imprimir pixel de conversión -->
      <?php if (isset($posted)) { ?>
      <?php if ($posted) { ?>
      <!-- Google Code for Convirtieron Deslactosada - Sentirse bien es contagioso- Conversion Page -->
      <script type="text/javascript">
      /* <![CDATA[ */
      var google_conversion_id = 982095385;
      var google_conversion_language = "en";
      var google_conversion_format = "3";
      var google_conversion_color = "ffffff";
      var google_conversion_label = "xT_uCMOEs14Qmaym1AM";
      var google_remarketing_only = false;
      /* ]]> */
      </script>
      <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
      </script>
      <noscript>
        <div style="display:inline;">
          <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/982095385/?label=xT_uCMOEs14Qmaym1AM&amp;guid=ON&amp;script=0"/>
        </div>
      </noscript>
      <?php } } ?>
      <!--script type="text/javascript" async src="//platform.twitter.com/widgets.js"></script-->
      <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-65013244-1', 'auto');
        ga('send', 'pageview');

    </script>
    <script>
      window.fbAsyncInit = function() {
        FB.init({
          appId      : '400928476768256',
          xfbml      : true,
          version    : 'v2.3'
        });
      };

      (function(d, s, id){
         var js, fjs = d.getElementsByTagName(s)[0];
         if (d.getElementById(id)) {return;}
         js = d.createElement(s); js.id = id;
         js.src = "//connect.facebook.net/es_ES/sdk.js";
         fjs.parentNode.insertBefore(js, fjs);
       }(document, 'script', 'facebook-jssdk'));
      </script>
       <?php if (isset($registered) && $registered) {?>

          <!-- Facebook Conversion Code for Registros - Deslactosada - Sentirse bien es contagioso- -->
          <script>(function() {
          var _fbq = window._fbq || (window._fbq = []);
          if (!_fbq.loaded) {
          var fbds = document.createElement('script');
          fbds.async = true;
          fbds.src = '//connect.facebook.net/en_US/fbds.js';
          var s = document.getElementsByTagName('script')[0];
          s.parentNode.insertBefore(fbds, s);
          _fbq.loaded = true;
          }
          })();
          window._fbq = window._fbq || [];
          window._fbq.push(['track', '6025004603322', {'value':'0.00','currency':'COP'}]);
          </script>
          <noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6025004603322&amp;cd[value]=0.00&amp;cd[currency]=COP&amp;noscript=1" /></noscript>
    <?php } ?>

    </head>
    <body class="internal">
      <?php if (isset($registered) && $registered) {?>
      <!-- Google Code for Convirtieron Deslactosada - Sentirse bien es contagioso- Conversion Page -->
      <script type="text/javascript">
      /* <![CDATA[ */
      var google_conversion_id = 982095385;
      var google_conversion_language = "en";
      var google_conversion_format = "3";
      var google_conversion_color = "ffffff";
      var google_conversion_label = "xT_uCMOEs14Qmaym1AM";
      var google_remarketing_only = false;
      /* ]]> */
      </script>
      <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
      </script>
      <noscript>
      <div style="display:inline;">
      <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/982095385/?label=xT_uCMOEs14Qmaym1AM&amp;guid=ON&amp;script=0"/>
      </div>
      </noscript>
    <?php } ?>
    <div id='fb-root'></div>
      <!--MAIN HEADER DESLCATOSADA-->
      <header id="mainHead">
        <div class="wrapHeader">
          <div id="mainCow" class="tweenFloat intCow"><img src="img/vaca_publiaciones.gif">
            <div id="bright"></div></div>
            <hgroup class="titleHead">
              <a href="/"><img src="img/header-title.png" /></a>
            </hgroup><!--/.wrapHeader-->
            <div class="netsSprt">
              <h3>¡Conéctate con la Vaca Madrina!</h3>
              <a href="https://twitter.com/lavacamadrina" onclick="ga('send', 'event', 'Home', 'Clic', '/Botón_Conoce_Twitter');" target="_blank" class="iconNet fbkIco">Twitter</a>
              <a href="https://www.facebook.com/profile.php?id=100009992106919&fref=nf" onclick="ga('send', 'event', 'Home', 'Clic', '/Botón_Conoce_Facebook');" target="_blank" class="iconNet twtIco">Facebook</a>
            </div>
          </div><!--/.wrapHeader-->
      </header><!--/#mainHead-->


      <!--CONTENTS-->
      <div class='container'>
        <div class='banner row'>

          <!--ALL GENERAL MODALS TO REVEAL-->

          <!--MODAL MESSAGE: WINNER - REWARD-->
          <article class="remodal messages-mdl reward-mdl" data-remodal-id="reward-message" data-remodal-options="hashTracking: false">
            <header class="header-modal-deslactosada">
              <hgroup><img src="img/header-title.png" /></hgroup>
            </header>
            <p class="intro">Eres la persona número</p>
            <figure id="counter">
              <ul data-count="<?php if (isset($position)) { echo $position; } else { echo 0;} ?>">
                <!--LIST are CREATED on JS--></ul>
              </figure>
              <p class="intro">en contagiar de bienestar.<br/></p>
              <h2 class="mdl-subtitles">¡Ganaste un premio sorpresa!</h2>
              <br/>
              <p>Para hacer el envío de tu premio<br/>debes déjanos tus datos a continuación.</p>
              <br/>
              <div class="ctas-mdl-wrap">
                <a onclick="displayRegisterForm(); ga('send', 'event', 'Sección_Ganador_5000', 'Clic', '/Botón_Regístrate_Ganador');" class="cta-btn-mdl-msg btn-blue">Regístrate</a>
                <!--a href="#" class="cta-btn-mdl-msg btn-red">Contagia más amigos</a-->
              </div><!--/.ctas-mdl-wrap-->
            </article><!--/.remodal.reward-mdl-->

            <!--MODAL MESSAGE: TRY AGAIN -->
            <article class="remodal messages-mdl tryagain-mdl" data-remodal-id="tryagain-message" data-remodal-options="hashTracking: true">
              <header class="header-modal-deslactosada">
                <hgroup><img src="img/header-title.png" /></hgroup>
              </header>
              <p class="intro">Eres la persona número</p>
              <figure id="counter">
              <ul data-count="<?php echo $user_count;?>">
                  <!--LIST are CREATED on JS--></ul>
                </figure>
                <p class="intro">en compartir mensajes de bienestar.</p><br/>
                <p>Sigue contagiando<br/>a tus amigos y</p>
                <h2 class="mdl-subtitles">¡Podrás recibir un premio sorpresa!</h2>
                <br/>
                <div class="ctas-mdl-wrap">
                  <a onclick="onConfirmClose(); ga('send', 'event', 'Sección_Sigue_Contagiando', 'Clic', '/Botón_Contagia_Más_Amigos');" class="cta-btn-mdl-msg btn-red">Contagia más amigos</a>
                </div><!--/.ctas-mdl-wrap-->
              </article><!--/.remodal.tryagain-mdl-->

              <!--MODAL MESSAGE: REGISTER SUCESS-->
              <article class="remodal messages-mdl success-register-mdl" data-remodal-id="success-register-message" data-remodal-options="hashTracking: true">
                <header class="header-modal-deslactosada">
                  <hgroup><img src="img/header-title.png" /></hgroup>
                </header>
                <h2 class="mdl-subtitles">!Muchas gracias!</h2>
                <br/>
                <p class="intro">Tus datos fueron<br/>registrados exitosamente.</p>
                <br/>
                <p>Continúa compartiendo mensajes<br/>para que contagies a más amigos.</p>
                <br/>
                <div class="ctas-mdl-wrap">
                  <a onclick="ga('send', 'event', 'Sección_Registro_Éxitoso', 'Clic', '/Botón_Seguir_Contagiando_Amigos');" href="#" class="cta-btn-mdl-msg btn-red">Contagia más amigos</a>
                </div><!--/.ctas-mdl-wrap-->
              </article><!--/.remodal.success-register-mdl-->

              <!--MODAL ACTION: CREATE AND REGISTER USER FORM-->
              <article class="remodal messages-mdl create-user-mdl" data-remodal-id="create-user-action" data-remodal-options="hashTracking: true">
                <header class="header-modal-deslactosada">
                  <hgroup><img src="img/header-title.png" /></hgroup>
                </header>
                <p class="intro">Estás a un paso</p>
                <h2 class="mdl-subtitles">de quedar registrado</h2>
                <br/>
                <p>Para hacer el envío del reconocimiento requerimos<br/>tus datos de ubicación</p>
                <form class="frm-deslactosada" method="POST" action="<?php echo base_url() ?>main/userRegistration" id="contact-form">
                  <fieldset class="inpts-frm">
                    <input type="text" placeholder="Nombres y Apellidos" name="name" required>
                  </fieldset>
                  <fieldset class="inpts-frm">
                    <input type="email" placeholder="e-Mail" name="email" required>
                  </fieldset>
                  <fieldset class="inpts-frm">
                  <!--<input type="text" class="toThree" placeholder="Día">
                  <input type="text" class="toThree" placeholder="Mes">
                  <input type="text" class="toThree" placeholder="Año">-->
                  <select class="toThree" name="birth_date_day" required>
                    <option value="">Día</option>
                    <?php for ($day = 1; $day <= 31; $day++) { ?>
                      <option value="<?php echo $day ?>"> <?php echo $day ?> </option>
                    <?php } ?>
                  </select>
                  <select class="toThree" name="birth_date_month" required>
                    <option value="">Mes</option>
                    <?php for ($month=1; $month <= 12 ; $month++) { ?>
                      <option value="<?php echo $month ?>"> <?php echo $month ?> </option>
                    <?php } ?>
                  </select>
                  <select class="toThree" name="birth_date_year" required>
                    <option value="">Año</option>
                    <?php for ($year=1935; $year <= 2015 ; $year++) { ?>
                      <option value="<?php echo $year ?>"> <?php echo $year ?></option>
                    <?php } ?>
                  </select>
                </fieldset>
                <fieldset class="inpts-frm">
                  <input type="text" placeholder="Dirección" name="address" required>
                </fieldset>
                <fieldset class="inpts-frm">
                  <select class="to100" name="city" required>
                    <option selected>Departamento - Ciudad</option>
                    <option value="Amazonas: Leticia"> Amazonas: Leticia</option>
                    <option value="Antioquia: Medellín">Antioquia: Medellín</option>
                    <option value="Arauca: Arauca">Arauca: Arauca</option>
                    <option value="Atlantico: Barranquilla">Atlantico: Barranquilla</option>
                    <option value="Bolivar: Cartagena">Bolivar: Cartagena</option>
                    <option value="Boyacá: Tunja">Boyacá: Tunja</option>
                    <option value="Caldas: Manizales">Caldas: Manizales</option>
                    <option value="Caquetá: Florencia">Caquetá: Florencia</option>
                    <option value="Casanare: Yopal">Casanare: Yopal</option>
                    <option value="Cauca: Popayán"> Cauca: Popayán</option>
                    <option value="Cesar: Valledupar"> Cesar: Valledupar</option>
                    <option value="Chocó: Quibdó"> Chocó: Quibdó</option>
                    <option value="Córdoba: Montería"> Córdoba: Montería</option>
                    <option value="Cundinamarca: Bogotá"> Cundinamarca: Bogotá</option>
                    <option value="Guainía: Puerto Inírida"> Guainía: Puerto Inírida</option>
                    <option value="Guaviare: San José del Guaviare"> Guaviare: San José del Guaviare</option>
                    <option value="Huila: Neiva"> Huila: Neiva</option>
                    <option value="La Guajira: Riohacha"> La Guajira: Riohacha</option>
                    <option value="Magdalena: Santa Marta"> Magdalena: Santa Marta</option>
                    <option value="Meta: Villavicencio"> Meta: Villavicencio</option>
                    <option value="Nariño: Pasto"> Nariño: Pasto</option>
                    <option value="Norte de Santander: Cúcuta"> Norte de Santander: Cúcuta</option>
                    <option value="Putumayo: Mocoa"> Putumayo: Mocoa</option>
                    <option value="Quindio: Armenia"> Quindio: Armenia</option>
                    <option value="Risaralda: Pereira"> Risaralda: Pereira</option>
                    <option value="San Andres y Providencia: San Andres"> San Andres y Providencia: San Andres</option>
                    <option value="Santander: Bucaramanga"> Santander: Bucaramanga</option>
                    <option value="Sucre: Sincelejo"> Sucre: Sincelejo</option>
                    <option value="Tolima: Ibagué"> Tolima: Ibagué</option>
                    <option value="Valle del Cauca: Cali"> Valle del Cauca: Cali</option>
                    <option value="Vaupés: Mitú"> Vaupés: Mitú</option>
                    <option value="Vichada: Puerto Carreño"> Vichada: Puerto Carreño</option>
                  </select>
                </fieldset>
                <fieldset class="inpts-frm">
                  <input type="text" placeholder="Teléfono" name="phone" required>
                </fieldset>
                <fieldset class="inpts-frm" style="text-align:center">
                  <label class="chckTrms">
                    <input type="checkbox" name="terms" required>Acepto los <a onclick="ga('send', 'event', 'Formulario', 'Clic', '/Botón_Términos_Y_Condiciones');" href="<?php echo base_url(); ?>terminos_y_condiciones_sentirse_bien_es_contagioso.pdf" target="_blank">términos y condiciones</a> del sitio
                  </label>
                </fieldset>
                <div class="ctas-mdl-wrap">
                <!--a href="#" class="cta-btn-mdl-msg btn-red more-width">Registrar datos de ubicación</a-->
                <input type="submit" value="Registrar datos de ubicación" class="cta-btn-mdl-msg btn-red more-width" onclick="ga('send', 'event', 'Sección_Registro', 'Clic', '/Botón_Registrar_Datos_De_Ubicación');">
              </div><!--/.ctas-mdl-wrap-->
              </form><!--/.deslactosada-->
              <!--div class="ctas-mdl-wrap">
                <a href="#" class="cta-btn-mdl-msg btn-red more-width">Registrar datos de ubicación</a>
              </div--><!--/.ctas-mdl-wrap-->
            </article><!--/.remodal.create-user-mdl-->

            <!--MODAL ACTION: SHARE AUDIO EMBED WITH MESSAGE-->
            <article class="remodal messages-mdl story-mdl-twt" data-remodal-id="audioToShare" data-remodal-options="hashTracking: true">
              <header class="header-modal-deslactosada">
                <hgroup><img src="img/header-title.png" /></hgroup>
              </header>
              <p class="intro">Menciona a los amigos que</p>
              <h2 class="mdl-subtitles">quieres contagiar de bienestar</h2>
              <div class="the-twit-message">
                <p>Sigue siendo alegre y siempre tendrás una sonrisa para regalar. <span class="hash">#SentirseBienEsContagioso</span> <!--a href="link">ly/Lm0Ty43</a--></p>
              </div>
              <!--AUDIO EMBED TO SHARE-->
              <figure class="audio-figure-toShare">
                <img src="img/histories-twiter-toAudio.jpg">
                <figcaption>
                  <div class="avatar-audio">
                    <img src="img/avatar-audio-dem.png"></div>
                    <audio class="mp3toAudioPlay" src="audios/sound.mp3">  
                      Tu navegador no soporta reproduccion de<code> audio </code> elementos.  
                    </audio>
                  </figcaption>
                </figure>
                <div class="ctas-mdl-wrap">
                  <a href="#" class="cta-btn-mdl-msg btn-red">Enviar mensaje</a>
                </div><!--/.ctas-mdl-wrap-->
              </article><!--/.remodal.messages-mdl.story-mdl-twt-->
      <!--END ALL MODALS-->
  