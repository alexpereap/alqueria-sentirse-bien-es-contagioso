	<!-- App -->
	<div id="app">
		<?php if (!$fb_session) {?>
		<!--MODAL ACTION: SHARE HISTORIE WITH TWITTER-->
          <article class="remodal messages-mdl story-mdl-twt" data-remodal-id="storyToShare-withTwt" data-remodal-options="hashTracking: true">
            <header class="header-modal-deslactosada">
              <hgroup><img src="img/header-title.png" /></hgroup>
            </header>
            <p class="intro">Menciona a los amigos que</p>
            <h2 class="mdl-subtitles">quieres contagiar de bienestar</h2>
            <div class="the-twit-message">
              <p>Sigue siendo alegre y siempre tendrás una sonrisa para regalar. <span class="hash">#SentirseBienEsContagioso</span> <a href="link">ly/Lm0Ty43</a></p>
            </div>
            <figure class="img-to-share share-with-twit">
              <img class="img-storyToShare-1" src="img/histories-01Dem-ToPost.jpg" alt="" />
            </figure>
            <div class="ctas-mdl-wrap"><!--more-width-->
              <a href="#" class="cta-btn-mdl-msg btn-red">Enviar mensaje</a>
            </div><!--/.ctas-mdl-wrap-->
          </article><!--/.remodal.messages-mdl.story-mdl-twt-->

        

		<a href="<?php echo $login_url?>">Facebook Login</a>
		<?php } else if($fb_session) { ?>

			
			<!--MODAL ACTION: SHARE HISTORIE WITH FACEBOOK-->
            <article class="remodal messages-mdl story-mdl-fbk" data-remodal-id="storyToShare-withFbk" data-remodal-options="hashTracking: true">
              <header class="header-modal-deslactosada">
                <hgroup><img src="img/header-title.png" /></hgroup>
              </header>
              <p class="intro">Selecciona a 10 de los amigos que</p>
              <h2 class="mdl-subtitles">quieres contagiar de bienestar</h2>
              <!--FORM TO SEARCH-->
              <form class="frm-deslactosada srch-frnds">
                <fieldset class="to-srch-friends">
                  <!--label class="lbl-frnds-slct">
                    <input type="checkbox" value="">
                    Seleccionar todos
                  </label-->
                  <div class="search-friend inpts-frm">
                    <input class="search-icon-input" type="text" placeholder="Escribe el nombre de tu amigo(a)">
                  </div>
                </fieldset>
              </form><!--/.frm-deslactosada.srch-frnds-->
              <!--FORM TO SELECT LISTED FRIENDS-->
                <!--TIP: APPEND DYNAMICALLY THE FACEBOOK LIST FRIEND WITH THIS STRUCTURE-->
                <?php if (isset($friends)) { ?>
					
			 <?php  foreach ($friends as $friend) { ?>
                <form class="tggbl-friends">
                  <div class="contentToScroll"><!--PREV WRAP-->
                    <fieldset>
                      <label class="tagFrnd">
                        <input type="checkbox" value="">
                        <div class="avtrFbk"><img src="<?php echo $friend['pic']?>" width="50" height="50"/></div>
                        <span class="nameFbk"><?php echo $friend['name']; ?></span>
                      </label><!--/.tagFrnd-->
                    </fieldset>
                  </div>
                </form><!--/.tggbl-friends-->
                <?php } ;?>
			
			<?php }?>
                <div class="ctas-mdl-wrap">
                  <a href="#" class="cta-btn-mdl-msg btn-red more-width">Enviar mensaje con FaceBook</a>
                </div><!--/.ctas-mdl-wrap-->
            </article><!--/.remodal.messages-mdl.story-mdl-fbk-->  
 
			<!-- start form-->
				<div class="box-lg-4 box-md-6">
					<div class="share-block">
						<form method="POST" enctype="multipart/form-data" id="formFb" action="<?php echo base_url();?>main/formFacebook">
							<p>
								<input style="display:none;" type="text" name="image_url" value="testImageShare.jpg">
								<img src="<?php echo base_url() ?>img/testImageShare.jpg">
							</p>
							<div class="subtitle">Tu comentario:</div>
							<textarea type="text" class="comment_user" name="comment_user" class="block-text"></textarea>
							<span class="validateComments"></span>
							<input type="submit" value="Compartir en Facebook">
						</form>
					</div>
				</div>
				
				<!-- end form-->
		

		<?php } else { ?>
			<p>paso algo inesperado</p>
	<?php } ?>

		<!--a href="<?php echo $login_url?>">Facebook Login</a>
		<a href="<?php echo base_url('twitter/auth');?>">Conéctate con twitter</a-->

		<!-- validacion de post twitter-->
			<?php 
			if(isset($tw_session))
			{
			if (!$tw_session && !$posted) { ?>
			<a href="<?php echo $login_url?>">Facebook Login</a>
			<a href="<?php echo base_url('twitter/auth');?>">Conéctate con twitter</a>
			<?php }else if (!$posted){ ?>

			<!-- state 2-->
		<div>
			<form method="POST" action="<?php echo base_url() ?>main/userRegistration" id="contact-form">
				<input type="text" name="name" placeholder="Escribe tu Nombre Completo" required>
				<input type="text" name="phone" placeholder="Celular o Telefono" class="number" required>
				<input type="text" name="birth_date_day" class="birth_date number" placeholder="Día" required>
				<input type="text" name="birth_date_month" class="birth_date number" placeholder="Mes" required>
				<input type="text" name="birth_date_year" class="birth_date number" placeholder="Año" required>
				<input type="text" name="address" placeholder="Dirección" required>
				<select name="city" required>
					<option value="">Seleccione</option>
					<option value="Bogota">Bogotá</option>
					<option value="Medellin">Medellin</option>
				</select>
				<input type="checkbox" name="terms" value="Y" required>
				<input type="submit" value="Enviar">
			</form>
		</div>		

			<?php } else if ($posted) {?>
			<!-- state 3 -->
			<div class="box-flex">
				<div class="box-lg-6 box-md-12">
					<div class="confirm2 bg-img2">
						<span class="icon-ok"></span>
						<div class="title">¡Tu publicación se ha<br>  realizado exitosamente!</div>
						<hr>
					</div>
				</div>
				<div class="box-lg-6 box-md-12">
					<div class="post">
						<p class="text">Ahora Comparte una frase en Facebook.</p>
						<a href="<?php echo $login_url ?>">
						<div class="btn-group">
							<div class="btn-default btn-fb shadow">
								<div class="icon"></div>
								<span class="text">Postear en Facebook</span>
							</div>
						</div>
						</a>
					</div>
				</div>
			</div>

			<?php } }?>

			<!-- final validaacion twitter-->

	</div>
	 