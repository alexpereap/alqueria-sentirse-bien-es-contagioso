
<!--index Old-->
  <!-- App -->
  <div id="app">
    <!--HISTORIES-->
        <section id="deslactosadaGrid" class="span twelve"> 
          <hgroup class="titFltr">
              <h1>Escoge el mensaje con el que quieres contagiar de bienestar a tus amigos,</h1>
              <h2>compártelo y podrás ganar un premio sorpresa.</h2>
            </hgroup>
            <!--NAVIGATION FILTERS-->
            <nav id="filterHeads">
              <ul class="filterCombo">
                <li class="btnFilter"><!--FILTER BY EMOJIS-->
                  <nav class="dropDown">
                    <a class="actvEmoji" href="#d"><span class="emoji emo-1"></span>Selecciona un estado</a>
                    <ul>
                      <li><a href="#" data-filter="emoji-positivo">
                        <span class="emoji emo-1" data-emoji="emo-1"></span>me siento positivo</a></li>
                      <li><a href="#" data-filter="emoji-inspirado">
                        <span class="emoji emo-2" data-emoji="emo-2"></span>me siento inspirado</a></li>
                      <li><a href="#" data-filter="emoji-feliz">
                        <span class="emoji emo-3" data-emoji="emo-3"></span>me siento feliz</a></li>
                      <li><a href="#" data-filter="emoji-alegre">
                        <span class="emoji emo-4" data-emoji="emo-4"></span>me siento alegre</a></li>
                      <li><a href="#" data-filter="emoji-entusiasmado">
                        <span class="emoji emo-5" data-emoji="emo-5"></span>me siento entusiasmado</a></li>
                      <li><a href="#" data-filter="emoji-genial">
                        <span class="emoji emo-6" data-emoji="emo-6"></span>me siento genial</a></li>
                      <li><a href="#" data-filter="emoji-optimista">
                        <span class="emoji emo-7" data-emoji="emo-7"></span>me siento optimista</a></li>
                      <li><a href="#" data-filter="emoji-orgulloso">
                        <span class="emoji emo-8" data-emoji="emo-8"></span>me siento orgulloso</a></li>
                      <li><a href="#" data-filter="emoji-maravillosamente">
                        <span class="emoji emo-9" data-emoji="emo-9"></span>me siento maravillosamente</a></li>
                      <li><a href="#" data-filter="emoji-super">
                        <span class="emoji emo-10" data-emoji="emo-10"></span>me siento súper</a></li>
                      <li><a href="#" data-filter="emoji-motivado">
                        <span class="emoji emo-11" data-emoji="emo-11"></span>me siento motivado</a></li>
                      <li><a href="#" data-filter="emoji-agradecido">
                        <span class="emoji emo-12" data-emoji="emo-12"></span>me siento agradecido</a></li>
                      <li><a href="#" data-filter="emoji-energia">
                        <span class="emoji emo-13" data-emoji="emo-13"></span>me siento lleno de energía</a></li>
                      <li><a href="#" data-filter="emoji-afortunado">
                        <span class="emoji emo-14" data-emoji="emo-14"></span>me siento afortunado</a></li>
                      <li><a href="#" data-filter="emoji-fantastico">
                        <span class="emoji emo-15" data-emoji="emo-15"></span>me siento fantástico</a></li>
                    </ul>
                  </nav>

                </li>
              </ul>
              <ul>
                <li class="btnFilter"><!--FILTER BY AUDIOS-->
                  <a href="#" data-filter="audio">Ver audios<span class="circ inctv"></span></a></li>
              </ul>
              <ul>
                <li class="btnFilter"><!--FILTER BY IMAGES-->
                  <a href="#" data-filter="image">Ver imágen<span class="circ inctv"></span></a></li>
              </ul>
              <ul>
                <li class="btnFilter"><!--VIEW ALL-->
                  <a href="#" data-filter="all">Ver todos<span class="circ inctv"></span></a></li>
              </ul>
              
            </nav><!--/#filterHeads-->

          <!--GRID HISTORIES PINS-->  
           <ul id="gridHistories">
              <li>
                <!--PIN STORY TO ACTIVATE MODAL-->
                  <figure class="story tipe-image tipe-emoji-alegre storyToShare-1">
                    <a class="to-reveal-modal" href="#storyToShare-1">
                        <img class="img-storyToShare-1" src="img/post/me_siento_alegre_3.jpg"></a>
                      <figcaption class="ctasOnHov">
                        <div class="boxPrnt">
                          <p class="boxChld">
                            <a href="#storyToShare-1" class="btn-toHov-mdl icoSrch" onclick="ga('send', 'event', 'Sección_Mensajes', 'Clic', '/Botón_Miralo_En_Detalle');">Míralo en detalle</a>
                            <!-- <a href="javascript:getShareLink('<?php echo base_url() ?>img/histories-01Dem-ToPost.jpg')" class="btn-toHov-mdl icoFbk" onclick="ga('send', 'event', 'Sección_Mensajes', 'Clic', '/Botón_Compartir_Facebook');">Compártelo</a> -->
                            <a href="javascript:getShareLink('<?php echo base_url() ?>img/post/me_siento_alegre_3.jpg','la vida es como un espejo, nos sonríe si la miramos sonriendo')" class="btn-toHov-mdl icoFbk" onclick="ga('send', 'event', 'Sección_Mensajes', 'Clic', '/Botón_Compartir_Facebook');">Compártelo</a>
                            <a href="<?php echo base_url('twitter/twitt/i1');?>" class="btn-toHov-mdl icoTwt" onclick="ga('send', 'event', 'Sección_Mensajes', 'Clic', '/Botón_Compartir_Twitter');">Twitéalo</a>
                          </p>
                        </div>
                      </figcaption>
                  </figure><!--/.story-->
                  <!--MODALS TO SHARE: TWO ACTIONS-->
                    <!--MIXED MODAL WITH CTAS TO REVEAL NETS-->
                    <article class="remodal story-mdl" data-remodal-id="storyToShare-1">
                      <!--POST IMAGE TO SHARE-->
                        <img class="img-storyToShare-1" src="img/post/me_siento_alegre_3.jpg" alt="" />
                      <!--MODAL CTA´s-->
                      <a onclick="ga('send', 'event', 'Sección_Compartir_Mensaje', 'Clic', '/Botón_Compartir_Mensaje_Facebook');" href="javascript:getShareLink('<?php echo base_url() ?>img/post/me_siento_alegre_3.jpg','la vida es como un espejo, nos sonríe si la miramos sonriendo')" class="btnModalToShare btn-fbk-ico cta-fbk-mdl">
                        <!--SHARE FaceBook-INTOMODAL-->
                        <strong>Compártelo</strong><span class="iconNet"></span></a>
                      <a onclick="ga('send', 'event', 'Sección_Compartir_Mensaje', 'Clic', '/Botón_Compartir_Mensaje_Twitter');" href="<?php echo base_url('twitter/twitt/i1');?>" class="btnModalToShare btn-twt-ico cta-twt-mdl">
                        <!--SHARE Twitter-INTOMODAL-->
                        <strong>Twitéalo</strong><span class="iconNet"></span></a>
                    </article><!--/.remodal.story-mdl-->
              </li>

             <li>
                  <figure class="story tipe-audio tipe-emoji-inspirado audioToShare-1">
                    <a class="to-reveal-modal" href="#audioToShare-1">
                      <img class="story-pin-img" src="img/audio/me_siento_afortunado_1.jpg"></a>
                      <figcaption class="ctasOnHov">
                        <div class="boxPrnt">
                          <p class="boxChld">
                            <a href="#audioToShare-1" class="btn-toHov-mdl icoSrch" onclick="ga('send', 'event', 'Sección_Mensajes', 'Clic', '/Botón_Miralo_En_Detalle');">Míralo en detalle</a>
                            <!--a href="https://www.facebook.com/sharer/sharer.php?u=https://www.facebook.com/100003158365642/videos/vb.100003158365642/810077782440826" class="btn-toHov-mdl icoFbk" onclick="ga('send', 'event', 'Sección_Mensajes', 'Clic', '/Botón_Compartir_Facebook');">Compártelo</a-->
                            <a href="javascript:getShareFB()" class="btn-toHov-mdl icoFbk" onclick="ga('send', 'event', 'Sección_Mensajes', 'Clic', '/Botón_Compartir_Facebook');">Compártelo</a>
                            <a href="<?php echo base_url('twitter/twitt/a1'); ?>" class="btn-toHov-mdl icoTwt" onclick="ga('send', 'event', 'Sección_Mensajes', 'Clic', '/Botón_Compartir_Twitter');">Twitéalo</a>
                          </p>
                        </div>
                      </figcaption>  
                  </figure>
                  <!--AUDIO EMBED TO PlAY-->
                    <div class="audio-embed-toplay">
                      <div class="avatar-audio">
                        <img src="img/avatar-audio-dem.png"></div>
                      <audio class="mp3embedAudioPlay" src="audios/sound.mp3">  
                        Tu navegador no soporta reproduccion de<code> audio </code> elementos.  
                      </audio>
                    </div>
                  <!--MODALS TO SHARE: PLAY AUDIO THIS POST-->
                    <!-- MODAL WITH CTAS TO SHARE WITH NETS-->
                    <article class="remodal story-mdl" data-remodal-id="audioToShare-1">
                      <!--AUDIO EMBED TO SHARE-->
                      <figure class="audio-figure-toShare">
                        <img src="img/histories-twiter-toAudio.jpg">
                        <figcaption>
                          <div class="avatar-audio">
                            <img src="img/avatar-audio-dem.png"></div>
                          <audio class="mp3toAudioPlay" src="audios/sound.mp3">  
                            Tu navegador no soporta reproduccion de<code> audio </code> elementos.  
                          </audio>
                        </figcaption>
                      </figure>
                        <!--img class="img-storyToShare-1" src="img/histories-01Dem-ToPost.jpg" alt="" -->
                      <!--MODAL CTA´s-->
                      <a onclick="ga('send', 'event', 'Sección_Compartir_Mensaje', 'Clic', '/Botón_Compartir_Audio_Facebook');" href="https://www.facebook.com/sharer/sharer.php?u=https://www.facebook.com/100003158365642/videos/vb.100003158365642/810077782440826" class="btnModalToShare btn-fbk-ico cta-fbk-mdl">
                        <!--SHARE FaceBook-INTOMODAL-->
                        <strong>Compártelo</strong><span class="iconNet"></span></a>
                      <a onclick="ga('send', 'event', 'Sección_Compartir_Mensaje', 'Clic', '/Botón_Compartir_Audio_Twitter');" href="<?php echo base_url('twitter/twitt/a1');?>" class="btnModalToShare btn-twt-ico cta-twt-mdl">
                        <!--SHARE Twitter-INTOMODAL-->
                        <strong>Twitéalo</strong><span class="iconNet"></span></a>
                    </article><!--/.remodal.story-mdl-->
              </li>
              <li>
                  <!--PIN STORY TO ACTIVATE MODAL-->
                  <figure class="story tipe-image tipe-emoji-feliz storyToShare-2">
                    <a class="to-reveal-modal" href="#storyToShare-2">
                        <img class="img-storyToShare-2" src="img/crops/mensajes_17.jpg"></a>
                      <figcaption class="ctasOnHov">
                        <div class="boxPrnt">
                          <p class="boxChld">
                            <a href="#storyToShare-2" class="btn-toHov-mdl icoSrch" onclick="ga('send', 'event', 'Sección_Mensajes', 'Clic', '/Botón_Miralo_En_Detalle');">Míralo en detalle</a>
                            <a href="javascript:getShareLink('<?php echo base_url ()?>img/post/me_siento_energia.jpg','Todo lo que queremos lograr está en nuestras manos y voluntad')" class="btn-toHov-mdl icoFbk" onclick="ga('send', 'event', 'Sección_Mensajes', 'Clic', '/Botón_Compartir_Facebook');">Compártelo</a>
                            <a href="<?php echo base_url('twitter/twitt/i2');?>" class="btn-toHov-mdl icoTwt" onclick="ga('send', 'event', 'Sección_Mensajes', 'Clic', '/Botón_Compartir_Twitter');">Twitéalo</a>
                          </p>
                        </div>
                      </figcaption>
                  </figure><!--/.story-->
                  <!--MODALS TO SHARE: TWO ACTIONS-->
                    <!--MIXED MODAL WITH CTAS TO REVEAL NETS-->
                    <article class="remodal story-mdl" data-remodal-id="storyToShare-2">
                      <!--POST IMAGE TO SHARE-->
                        <img class="img-storyToShare-2" src="img/crops/mensajes_17.jpg" alt="" />
                      <!--MODAL CTA´s-->
                      <a onclick="ga('send', 'event', 'Sección_Compartir_Mensaje', 'Clic', '/Botón_Compartir_Mensaje_Facebook');" href="javascript:getShareLink('<?php echo base_url ()?>img/post/me_siento_energia.jpg','Todo lo que queremos lograr está en nuestras manos y voluntad')" class="btnModalToShare btn-fbk-ico cta-fbk-mdl">
                        <!--SHARE FaceBook-INTOMODAL-->
                        <strong>Compártelo</strong><span class="iconNet"></span></a>
                      <a onclick="ga('send', 'event', 'Sección_Compartir_Mensaje', 'Clic', '/Botón_Compartir_Mensaje_Twitter');" href="<?php echo base_url('twitter/twitt/i2');?>" class="btnModalToShare btn-twt-ico cta-twt-mdl">
                        <!--SHARE Twitter-INTOMODAL-->
                        <strong>Twitéalo</strong><span class="iconNet"></span></a>
                    </article><!--/.remodal.story-mdl-->
              </li>
              <li>
                  <!--PIN STORY TO ACTIVATE MODAL-->
                  <figure class="story tipe-audio tipe-emoji-alegre storyToShare-3">
                    <a class="to-reveal-modal" href="#storyToShare-3">
                        <img class="img-storyToShare-3" src="img/crops/mensajes_57.jpg"></a>
                      <figcaption class="ctasOnHov">
                        <div class="boxPrnt">
                          <p class="boxChld">
                            <a href="#storyToShare-3" class="btn-toHov-mdl icoSrch" onclick="ga('send', 'event', 'Sección_Mensajes', 'Clic', '/Botón_Miralo_En_Detalle');">Míralo en detalle</a>
                            <a href="javascript:getShareLink('<?php echo base_url() ?>img/crops/mensajes_57.jpg','Eres como un reloj, siempre hacia delante, contagia a los demás de esta gran cualidad')" class="btn-toHov-mdl icoFbk" onclick="ga('send', 'event', 'Sección_Mensajes', 'Clic', '/Botón_Compartir_Facebook');">Compártelo</a>
                            <a href="<?php echo base_url('twitter/twitt/i3');?>" class="btn-toHov-mdl icoTwt" onclick="ga('send', 'event', 'Sección_Mensajes', 'Clic', '/Botón_Compartir_Twitter');">Twitéalo</a>
                          </p>
                        </div>
                      </figcaption>
                  </figure><!--/.story-->
                  <!--AUDIO EMBED TO PlAY-->
                    <div class="audio-embed-toplay">
                      <div class="avatar-audio">
                        <img src="img/avatar-audio-dem.png"></div>
                      <audio class="mp3embedAudioPlay" src="audios/sound.mp3">  
                        Tu navegador no soporta reproduccion de<code> audio </code> elementos.  
                      </audio>
                    </div>
                  <!--MODALS TO SHARE: TWO ACTIONS-->
                    <!--MIXED MODAL WITH CTAS TO REVEAL NETS-->
                    <article class="remodal story-mdl" data-remodal-id="storyToShare-3">
                      <!--POST IMAGE TO SHARE-->
                        <img class="img-storyToShare-3" src="img/crops/mensajes_57.jpg" alt="" />
                      <!--MODAL CTA´s-->
                      <a onclick="ga('send', 'event', 'Sección_Compartir_Mensaje', 'Clic', '/Botón_Compartir_Mensaje_Facebook');" href="javascript:getShareLink('<?php echo base_url() ?>img/crops/mensajes_57.jpg','Eres como un reloj, siempre hacia delante, contagia a los demás de esta gran cualidad')" class="btnModalToShare btn-fbk-ico cta-fbk-mdl">
                        <!--SHARE FaceBook-INTOMODAL-->
                        <strong>Compártelo</strong><span class="iconNet"></span></a>
                      <a onclick="ga('send', 'event', 'Sección_Compartir_Mensaje', 'Clic', '/Botón_Compartir_Mensaje_Twitter');" href="<?php echo base_url('twitter/twitt/i3');?>" class="btnModalToShare btn-twt-ico cta-twt-mdl">
                        <!--SHARE Twitter-INTOMODAL-->
                        <strong>Twitéalo</strong><span class="iconNet"></span></a>
                    </article><!--/.remodal.story-mdl-->
              </li>
              <li>
                  <figure class="story tipe-audio tipe-emoji-genial audioToShare-2">
                    <a class="to-reveal-modal" href="#audioToShare-2">
                      <img class="story-pin-img" src="img/histories-twiter-toAudio.jpg"></a>
                      <figcaption class="ctasOnHov">
                        <div class="boxPrnt">
                          <p class="boxChld">
                            <a href="#audioToShare-2" class="btn-toHov-mdl icoSrch" onclick="ga('send', 'event', 'Sección_Mensajes', 'Clic', '/Botón_Miralo_En_Detalle');">Míralo en detalle</a>
                            <a href="https://www.facebook.com/sharer/sharer.php?u=https://www.facebook.com/100003158365642/videos/vb.100003158365642/810077782440826" class="btn-toHov-mdl icoFbk" onclick="ga('send', 'event', 'Sección_Mensajes', 'Clic', '/Botón_Compartir_Facebook');">Compártelo</a>
                            <a href="<?php echo base_url('twitter/twitt/a2');?>" class="btn-toHov-mdl icoTwt" onclick="ga('send', 'event', 'Sección_Mensajes', 'Clic', '/Botón_Compartir_Twitter');">Twitéalo</a>
                          </p>
                        </div>
                      </figcaption>  
                  </figure>
                  <!--AUDIO EMBED TO PlAY-->
                    <div class="audio-embed-toplay">
                      <div class="avatar-audio">
                        <img src="img/avatar-audio-dem.png"></div>
                      <audio class="mp3embedAudioPlay" src="audios/sound.mp3">  
                        Tu navegador no soporta reproduccion de<code> audio </code> elementos.  
                      </audio>
                    </div>
                  <!--MODALS TO SHARE: PLAY AUDIO THIS POST-->
                    <!-- MODAL WITH CTAS TO SHARE WITH NETS-->
                    <article class="remodal story-mdl" data-remodal-id="audioToShare-2">
                      <!--AUDIO EMBED TO SHARE-->
                      <figure class="audio-figure-toShare">
                        <img src="img/histories-twiter-toAudio.jpg">
                        <figcaption>
                          <div class="avatar-audio">
                            <img src="img/avatar-audio-dem.png"></div>
                          <audio class="mp3toAudioPlay" src="audios/sound.mp3">  
                            Tu navegador no soporta reproduccion de<code> audio </code> elementos.  
                          </audio>
                        </figcaption>
                      </figure>
                        <!--img class="img-storyToShare-1" src="img/histories-01Dem-ToPost.jpg" alt="" -->
                      <!--MODAL CTA´s-->
                      <a onclick="ga('send', 'event', 'Sección_Compartir_Mensaje', 'Clic', '/Botón_Compartir_Audio_Facebook');" href="https://www.facebook.com/sharer/sharer.php?u=https://www.facebook.com/100003158365642/videos/vb.100003158365642/810077782440826" class="btnModalToShare btn-fbk-ico cta-fbk-mdl">
                        <!--SHARE FaceBook-INTOMODAL-->
                        <strong>Compártelo</strong><span class="iconNet"></span></a>
                      <a onclick="ga('send', 'event', 'Sección_Compartir_Mensaje', 'Clic', '/Botón_Compartir_Audio_Twitter');" href="<?php echo base_url('twitter/twitt/a2');?>" class="btnModalToShare btn-twt-ico cta-twt-mdl">
                        <!--SHARE Twitter-INTOMODAL-->
                        <strong>Twitéalo</strong><span class="iconNet"></span></a>
                    </article><!--/.remodal.story-mdl-->
              </li>
              <!--SIMPLE AND PLAIN IMAGS WITHOUT MODALS-->
              <li>
                  <!--PIN STORY TO ACTIVATE MODAL-->
                  <figure class="story tipe-image storyToShare-4">
                    <a class="to-reveal-modal" href="#storyToShare-4">
                        <img class="img-storyToShare-4" src="img/crops/mensajes_07.jpg"></a>
                      <figcaption class="ctasOnHov">
                        <div class="boxPrnt">
                          <p class="boxChld">
                            <a href="#storyToShare-4" class="btn-toHov-mdl icoSrch" onclick="ga('send', 'event', 'Sección_Mensajes', 'Clic', '/Botón_Miralo_En_Detalle');">Míralo en detalle</a>
                            <a href="javascript:getShareLink('<?php echo base_url() ?>img/crops/mensajes_07.jpg','Hoy es tu día; si lo puedes soñar, lo puedes hacer. Contagia  a todos con tu actitud')" class="btn-toHov-mdl icoFbk" onclick="ga('send', 'event', 'Sección_Mensajes', 'Clic', '/Botón_Compartir_Facebook');">Compártelo</a>
                            <a href="<?php echo base_url('twitter/twitt/i4');?>" class="btn-toHov-mdl icoTwt" onclick="ga('send', 'event', 'Sección_Mensajes', 'Clic', '/Botón_Compartir_Twitter');">Twitéalo</a>
                          </p>
                        </div>
                      </figcaption>
                  </figure><!--/.story-->
                  <!--MODALS TO SHARE: TWO ACTIONS-->
                    <!--MIXED MODAL WITH CTAS TO REVEAL NETS-->
                    <article class="remodal story-mdl" data-remodal-id="storyToShare-4">
                      <!--POST IMAGE TO SHARE-->
                        <img class="img-storyToShare-4" src="img/crops/mensajes_07.jpg" alt="" />
                      <!--MODAL CTA´s-->
                      <a onclick="ga('send', 'event', 'Sección_Compartir_Mensaje', 'Clic', '/Botón_Compartir_Mensaje_Facebook');" href="javascript:getShareLink('<?php echo base_url() ?>img/crops/mensajes_07.jpg','Hoy es tu día; si lo puedes soñar, lo puedes hacer. Contagia  a todos con tu actitud')" class="btnModalToShare btn-fbk-ico cta-fbk-mdl">
                        <!--SHARE FaceBook-INTOMODAL-->
                        <strong>Compártelo</strong><span class="iconNet"></span></a>
                      <a onclick="ga('send', 'event', 'Sección_Compartir_Mensaje', 'Clic', '/Botón_Compartir_Mensaje_Twitter');" href="<?php echo base_url('twitter/twitt/i4');?>" class="btnModalToShare btn-twt-ico cta-twt-mdl">
                        <!--SHARE Twitter-INTOMODAL-->
                        <strong>Twitéalo</strong><span class="iconNet"></span></a>
                    </article><!--/.remodal.story-mdl-->
              </li>

              <li>
                  <!--PIN STORY TO ACTIVATE MODAL-->
                  <figure class="story storyToShare-5">
                    <a class="to-reveal-modal" href="#storyToShare-5">
                        <img class="img-storyToShare-5" src="img/crops/mensajes_19.jpg"></a>
                      <figcaption class="ctasOnHov">
                        <div class="boxPrnt">
                          <p class="boxChld">
                            <a href="#storyToShare-5" class="btn-toHov-mdl icoSrch" onclick="ga('send', 'event', 'Sección_Mensajes', 'Clic', '/Botón_Miralo_En_Detalle');">Míralo en detalle</a>
                            <a href="javascript:getShareLink('<?php echo base_url() ?>img/crops/mensajes_19.jpg','Sigue siendo alegre y siempre tendrás una sonrisa para regalar, sal y compártela')" class="btn-toHov-mdl icoFbk" onclick="ga('send', 'event', 'Sección_Mensajes', 'Clic', '/Botón_Compartir_Facebook');">Compártelo</a>
                            <a href="<?php echo base_url('twitter/twitt/i5');?>" class="btn-toHov-mdl icoTwt" onclick="ga('send', 'event', 'Sección_Mensajes', 'Clic', '/Botón_Compartir_Twitter');">Twitéalo</a>
                          </p>
                        </div>
                      </figcaption>
                  </figure><!--/.story-->
                  <!--MODALS TO SHARE: TWO ACTIONS-->
                    <!--MIXED MODAL WITH CTAS TO REVEAL NETS-->
                    <article class="remodal story-mdl" data-remodal-id="storyToShare-5">
                      <!--POST IMAGE TO SHARE-->
                        <img class="img-storyToShare-5" src="img/crops/mensajes_19.jpg" alt="" />
                      <!--MODAL CTA´s-->
                      <a onclick="ga('send', 'event', 'Sección_Compartir_Mensaje', 'Clic', '/Botón_Compartir_Mensaje_Facebook');" href="javascript:getShareLink('<?php echo base_url() ?>img/crops/mensajes_19.jpg','Sigue siendo alegre y siempre tendrás una sonrisa para regalar, sal y compártela')" class="btnModalToShare btn-fbk-ico cta-fbk-mdl">
                        <!--SHARE FaceBook-INTOMODAL-->
                        <strong>Compártelo</strong><span class="iconNet"></span></a>
                      <a onclick="ga('send', 'event', 'Sección_Compartir_Mensaje', 'Clic', '/Botón_Compartir_Mensaje_Twitter');" href="<?php echo base_url('twitter/twitt/i5');?>" class="btnModalToShare btn-twt-ico cta-twt-mdl">
                        <!--SHARE Twitter-INTOMODAL-->
                        <strong>Twitéalo</strong><span class="iconNet"></span></a>
                    </article><!--/.remodal.story-mdl-->
              </li>

              <li>
                  <!--PIN STORY TO ACTIVATE MODAL-->
                  <figure class="story storyToShare-6">
                    <a class="to-reveal-modal" href="#storyToShare-6">
                        <img class="img-storyToShare-6" src="img/crops/mensajes_27.jpg"></a>
                      <figcaption class="ctasOnHov">
                        <div class="boxPrnt">
                          <p class="boxChld">
                            <a href="#storyToShare-6" class="btn-toHov-mdl icoSrch" onclick="ga('send', 'event', 'Sección_Mensajes', 'Clic', '/Botón_Miralo_En_Detalle');">Míralo en detalle</a>
                            <a href="javascript:getShareLink('<?php echo base_url() ?>img/crops/mensajes_27.jpg','El entusiasmo es tan importante que hace una gran diferencia en tu día, contágialo')" class="btn-toHov-mdl icoFbk" onclick="ga('send', 'event', 'Sección_Mensajes', 'Clic', '/Botón_Compartir_Facebook');">Compártelo</a>
                            <a href="<?php echo base_url('twitter/twitt/i6');?>" class="btn-toHov-mdl icoTwt" onclick="ga('send', 'event', 'Sección_Mensajes', 'Clic', '/Botón_Compartir_Twitter');">Twitéalo</a>
                          </p>
                        </div>
                      </figcaption>
                  </figure><!--/.story-->
                  <!--MODALS TO SHARE: TWO ACTIONS-->
                    <!--MIXED MODAL WITH CTAS TO REVEAL NETS-->
                    <article class="remodal story-mdl" data-remodal-id="storyToShare-6">
                      <!--POST IMAGE TO SHARE-->
                        <img class="img-storyToShare-6" src="img/crops/mensajes_27.jpg" alt="" />
                      <!--MODAL CTA´s-->
                      <a onclick="ga('send', 'event', 'Sección_Compartir_Mensaje', 'Clic', '/Botón_Compartir_Mensaje_Facebook');" href="javascript:getShareLink('<?php echo base_url() ?>img/crops/mensajes_27.jpg','El entusiasmo es tan importante que hace una gran diferencia en tu día, contágialo')" class="btnModalToShare btn-fbk-ico cta-fbk-mdl">
                        <!--SHARE FaceBook-INTOMODAL-->
                        <strong>Compártelo</strong><span class="iconNet"></span></a>
                      <a onclick="ga('send', 'event', 'Sección_Compartir_Mensaje', 'Clic', '/Botón_Compartir_Mensaje_Twitter');" href="<?php echo base_url('twitter/twitt/i6');?>" class="btnModalToShare btn-twt-ico cta-twt-mdl">
                        <!--SHARE Twitter-INTOMODAL-->
                        <strong>Twitéalo</strong><span class="iconNet"></span></a>
                    </article><!--/.remodal.story-mdl-->
              </li>

              <li>
                  <!--PIN STORY TO ACTIVATE MODAL-->
                  <figure class="story storyToShare-7">
                    <a class="to-reveal-modal" href="#storyToShare-7">
                        <img class="img-storyToShare-7" src="img/crops/mensajes_29.jpg"></a>
                      <figcaption class="ctasOnHov">
                        <div class="boxPrnt">
                          <p class="boxChld">
                            <a href="#storyToShare-7" class="btn-toHov-mdl icoSrch" onclick="ga('send', 'event', 'Sección_Mensajes', 'Clic', '/Botón_Miralo_En_Detalle');">Míralo en detalle</a>
                            <a href="javascript:getShareLink('<?php echo base_url() ?>img/crops/mensajes_29.jpg','Si tienes una actitud positiva no solo estarás cambiando tu día, sino el de las personas que te rodean')" class="btn-toHov-mdl icoFbk" onclick="ga('send', 'event', 'Sección_Mensajes', 'Clic', '/Botón_Compartir_Facebook');">Compártelo</a>
                            <a href="<?php echo base_url('twitter/twitt/i7');?>" class="btn-toHov-mdl icoTwt" onclick="ga('send', 'event', 'Sección_Mensajes', 'Clic', '/Botón_Compartir_Twitter');">Twitéalo</a>
                          </p>
                        </div>
                      </figcaption>
                  </figure><!--/.story-->
                  <!--MODALS TO SHARE: TWO ACTIONS-->
                    <!--MIXED MODAL WITH CTAS TO REVEAL NETS-->
                    <article class="remodal story-mdl" data-remodal-id="storyToShare-7">
                      <!--POST IMAGE TO SHARE-->
                        <img class="img-storyToShare-7" src="img/crops/mensajes_29.jpg" alt="" />
                      <!--MODAL CTA´s-->
                      <a onclick="ga('send', 'event', 'Sección_Compartir_Mensaje', 'Clic', '/Botón_Compartir_Mensaje_Facebook');" href="javascript:getShareLink('<?php echo base_url() ?>img/crops/mensajes_29.jpg','Si tienes una actitud positiva no solo estarás cambiando tu día, sino el de las personas que te rodean')" class="btnModalToShare btn-fbk-ico cta-fbk-mdl">
                        <!--SHARE FaceBook-INTOMODAL-->
                        <strong>Compártelo</strong><span class="iconNet"></span></a>
                      <a onclick="ga('send', 'event', 'Sección_Compartir_Mensaje', 'Clic', '/Botón_Compartir_Mensaje_Twitter');" href="<?php echo base_url('twitter/twitt/i7');?>" class="btnModalToShare btn-twt-ico cta-twt-mdl">
                        <!--SHARE Twitter-INTOMODAL-->
                        <strong>Twitéalo</strong><span class="iconNet"></span></a>
                    </article><!--/.remodal.story-mdl-->
              </li>

              <li>
                  <!--PIN STORY TO ACTIVATE MODAL-->
                  <figure class="story storyToShare-8">
                    <a class="to-reveal-modal" href="#storyToShare-8">
                        <img class="img-storyToShare-8" src="img/crops/mensajes_37.jpg"></a>
                      <figcaption class="ctasOnHov">
                        <div class="boxPrnt">
                          <p class="boxChld">
                            <a href="#storyToShare-8" class="btn-toHov-mdl icoSrch" onclick="ga('send', 'event', 'Sección_Mensajes', 'Clic', '/Botón_Miralo_En_Detalle');">Míralo en detalle</a>
                            <a href="javascript:getShareLink('<?php echo base_url() ?>img/crops/mensajes_37.jpg','Para hacer acciones positivas hay que tener mente positiva')" class="btn-toHov-mdl icoFbk" onclick="ga('send', 'event', 'Sección_Mensajes', 'Clic', '/Botón_Compartir_Facebook');">Compártelo</a>
                            <a href="<?php echo base_url('twitter/twitt/i8');?>" class="btn-toHov-mdl icoTwt" onclick="ga('send', 'event', 'Sección_Mensajes', 'Clic', '/Botón_Compartir_Twitter');">Twitéalo</a>
                          </p>
                        </div>
                      </figcaption>
                  </figure><!--/.story-->
                  <!--MODALS TO SHARE: TWO ACTIONS-->
                    <!--MIXED MODAL WITH CTAS TO REVEAL NETS-->
                    <article class="remodal story-mdl" data-remodal-id="storyToShare-8">
                      <!--POST IMAGE TO SHARE-->
                        <img class="img-storyToShare-8" src="img/crops/mensajes_37.jpg" alt="" />
                      <!--MODAL CTA´s-->
                      <a onclick="ga('send', 'event', 'Sección_Compartir_Mensaje', 'Clic', '/Botón_Compartir_Mensaje_Facebook');" href="javascript:getShareLink('<?php echo base_url() ?>img/crops/mensajes_37.jpg','Para hacer acciones positivas hay que tener mente positiva')" class="btnModalToShare btn-fbk-ico cta-fbk-mdl">
                        <!--SHARE FaceBook-INTOMODAL-->
                        <strong>Compártelo</strong><span class="iconNet"></span></a>
                      <a onclick="ga('send', 'event', 'Sección_Compartir_Mensaje', 'Clic', '/Botón_Compartir_Mensaje_Twitter');" href="<?php echo base_url('twitter/twitt/i8');?>" class="btnModalToShare btn-twt-ico cta-twt-mdl">
                        <!--SHARE Twitter-INTOMODAL-->
                        <strong>Twitéalo</strong><span class="iconNet"></span></a>
                    </article><!--/.remodal.story-mdl-->
              </li>

              <li>
                  <!--PIN STORY TO ACTIVATE MODAL-->
                  <figure class="story storyToShare-9">
                    <a class="to-reveal-modal" href="#storyToShare-9">
                        <img class="img-storyToShare-9" src="img/crops/mensajes_39.jpg"></a>
                      <figcaption class="ctasOnHov">
                        <div class="boxPrnt">
                          <p class="boxChld">
                            <a href="#storyToShare-9" class="btn-toHov-mdl icoSrch" onclick="ga('send', 'event', 'Sección_Mensajes', 'Clic', '/Botón_Miralo_En_Detalle');">Míralo en detalle</a>
                            <a href="javascript:getShareLink('<?php echo base_url() ?>img/crops/mensajes_39.jpg','Infla tu corazón de orgullo y compártelo con los que siempre están ahí')" class="btn-toHov-mdl icoFbk" onclick="ga('send', 'event', 'Sección_Mensajes', 'Clic', '/Botón_Compartir_Facebook');">Compártelo</a>
                            <a href="<?php echo base_url('twitter/twitt/i9');?>" class="btn-toHov-mdl icoTwt" onclick="ga('send', 'event', 'Sección_Mensajes', 'Clic', '/Botón_Compartir_Twitter');">Twitéalo</a>
                          </p>
                        </div>
                      </figcaption>
                  </figure><!--/.story-->
                  <!--MODALS TO SHARE: TWO ACTIONS-->
                    <!--MIXED MODAL WITH CTAS TO REVEAL NETS-->
                    <article class="remodal story-mdl" data-remodal-id="storyToShare-9">
                      <!--POST IMAGE TO SHARE-->
                        <img class="img-storyToShare-9" src="img/crops/mensajes_39.jpg" alt="" />
                      <!--MODAL CTA´s-->
                      <a onclick="ga('send', 'event', 'Sección_Compartir_Mensaje', 'Clic', '/Botón_Compartir_Mensaje_Facebook');" href="javascript:getShareLink('<?php echo base_url() ?>img/crops/mensajes_39.jpg','Infla tu corazón de orgullo y compártelo con los que siempre están ahí')" class="btnModalToShare btn-fbk-ico cta-fbk-mdl">
                        <!--SHARE FaceBook-INTOMODAL-->
                        <strong>Compártelo</strong><span class="iconNet"></span></a>
                      <a onclick="ga('send', 'event', 'Sección_Compartir_Mensaje', 'Clic', '/Botón_Compartir_Mensaje_Twitter');" href="<?php echo base_url('twitter/twitt/i9');?>" class="btnModalToShare btn-twt-ico cta-twt-mdl">
                        <!--SHARE Twitter-INTOMODAL-->
                        <strong>Twitéalo</strong><span class="iconNet"></span></a>
                    </article><!--/.remodal.story-mdl-->
              </li>

              <li>
                  <!--PIN STORY TO ACTIVATE MODAL-->
                  <figure class="story storyToShare-10">
                    <a class="to-reveal-modal" href="#storyToShare-10">
                        <img class="img-storyToShare-10" src="img/crops/mensajes_47.jpg"></a>
                      <figcaption class="ctasOnHov">
                        <div class="boxPrnt">
                          <p class="boxChld">
                            <a href="#storyToShare-10" class="btn-toHov-mdl icoSrch" onclick="ga('send', 'event', 'Sección_Mensajes', 'Clic', '/Botón_Miralo_En_Detalle');">Míralo en detalle</a>
                            <a href="javascript:getShareLink('<?php echo base_url() ?>img/crops/mensajes_47.jpg','Es la recompensa de ser bueno y hacer bien, sentirse así es contagioso')" class="btn-toHov-mdl icoFbk" onclick="ga('send', 'event', 'Sección_Mensajes', 'Clic', '/Botón_Compartir_Facebook');">Compártelo</a>
                            <a href="<?php echo base_url('twitter/twitt/i10');?>" class="btn-toHov-mdl icoTwt" onclick="ga('send', 'event', 'Sección_Mensajes', 'Clic', '/Botón_Compartir_Twitter');">Twitéalo</a>
                          </p>
                        </div>
                      </figcaption>
                  </figure><!--/.story-->
                  <!--MODALS TO SHARE: TWO ACTIONS-->
                    <!--MIXED MODAL WITH CTAS TO REVEAL NETS-->
                    <article class="remodal story-mdl" data-remodal-id="storyToShare-10">
                      <!--POST IMAGE TO SHARE-->
                        <img class="img-storyToShare-10" src="img/crops/mensajes_47.jpg" alt="" />
                      <!--MODAL CTA´s-->
                      <a onclick="ga('send', 'event', 'Sección_Compartir_Mensaje', 'Clic', '/Botón_Compartir_Mensaje_Facebook');" href="javascript:getShareLink('<?php echo base_url() ?>img/crops/mensajes_47.jpg','Es la recompensa de ser bueno y hacer bien, sentirse así es contagioso')" class="btnModalToShare btn-fbk-ico cta-fbk-mdl">
                        <!--SHARE FaceBook-INTOMODAL-->
                        <strong>Compártelo</strong><span class="iconNet"></span></a>
                      <a onclick="ga('send', 'event', 'Sección_Compartir_Mensaje', 'Clic', '/Botón_Compartir_Mensaje_Twitter');" href="<?php echo base_url('twitter/twitt/i10');?>" class="btnModalToShare btn-twt-ico cta-twt-mdl">
                        <!--SHARE Twitter-INTOMODAL-->
                        <strong>Twitéalo</strong><span class="iconNet"></span></a>
                    </article><!--/.remodal.story-mdl-->
              </li>

              <li>
                  <!--PIN STORY TO ACTIVATE MODAL-->
                  <figure class="story storyToShare-11">
                    <a class="to-reveal-modal" href="#storyToShare-11">
                        <img class="img-storyToShare-11" src="img/crops/mensajes_49.jpg"></a>
                      <figcaption class="ctasOnHov">
                        <div class="boxPrnt">
                          <p class="boxChld">
                            <a href="#storyToShare-11" class="btn-toHov-mdl icoSrch" onclick="ga('send', 'event', 'Sección_Mensajes', 'Clic', '/Botón_Miralo_En_Detalle');">Míralo en detalle</a>
                            <a href="javascript:getShareLink('<?php echo base_url() ?>img/crops/mensajes_49.jpg','Hoy el héroe eres tú, sigue siendo invencible compártelo con los que te rodean')" class="btn-toHov-mdl icoFbk" onclick="ga('send', 'event', 'Sección_Mensajes', 'Clic', '/Botón_Compartir_Facebook');">Compártelo</a>
                            <a href="<?php echo base_url('twitter/twitt/i11');?>" class="btn-toHov-mdl icoTwt" onclick="ga('send', 'event', 'Sección_Mensajes', 'Clic', '/Botón_Compartir_Twitter');">Twitéalo</a>
                          </p>
                        </div>
                      </figcaption>
                  </figure><!--/.story-->
                  <!--MODALS TO SHARE: TWO ACTIONS-->
                    <!--MIXED MODAL WITH CTAS TO REVEAL NETS-->
                    <article class="remodal story-mdl" data-remodal-id="storyToShare-11">
                      <!--POST IMAGE TO SHARE-->
                        <img class="img-storyToShare-11" src="img/crops/mensajes_49.jpg" alt="" />
                      <!--MODAL CTA´s-->
                      <a onclick="ga('send', 'event', 'Sección_Compartir_Mensaje', 'Clic', '/Botón_Compartir_Mensaje_Facebook');" href="javascript:getShareLink('<?php echo base_url() ?>img/crops/mensajes_49.jpg','Hoy el héroe eres tú, sigue siendo invencible compártelo con los que te rodean')" class="btnModalToShare btn-fbk-ico cta-fbk-mdl">
                        <!--SHARE FaceBook-INTOMODAL-->
                        <strong>Compártelo</strong><span class="iconNet"></span></a>
                      <a onclick="ga('send', 'event', 'Sección_Compartir_Mensaje', 'Clic', '/Botón_Compartir_Mensaje_Twitter');" href="<?php echo base_url('twitter/twitt/i11');?>" class="btnModalToShare btn-twt-ico cta-twt-mdl">
                        <!--SHARE Twitter-INTOMODAL-->
                        <strong>Twitéalo</strong><span class="iconNet"></span></a>
                    </article><!--/.remodal.story-mdl-->
              </li>

              <li>
                  <!--PIN STORY TO ACTIVATE MODAL-->
                  <figure class="story storyToShare-12">
                    <a class="to-reveal-modal" href="#storyToShare-12">
                        <img class="img-storyToShare-12" src="img/crops/mensajes_59.jpg"></a>
                      <figcaption class="ctasOnHov">
                        <div class="boxPrnt">
                          <p class="boxChld">
                            <a href="#storyToShare-12" class="btn-toHov-mdl icoSrch" onclick="ga('send', 'event', 'Sección_Mensajes', 'Clic', '/Botón_Miralo_En_Detalle');">Míralo en detalle</a>
                            <a href="javascript:getShareLink('<?php echo base_url() ?>img/crops/mensajes_59.jpg','La gratitud le da sentido al pasado y la paz al presente, compártela')" class="btn-toHov-mdl icoFbk" onclick="ga('send', 'event', 'Sección_Mensajes', 'Clic', '/Botón_Compartir_Facebook');">Compártelo</a>
                            <a href="<?php echo base_url('twitter/twitt/i12');?>" class="btn-toHov-mdl icoTwt" onclick="ga('send', 'event', 'Sección_Mensajes', 'Clic', '/Botón_Compartir_Twitter');">Twitéalo</a>
                          </p>
                        </div>
                      </figcaption>
                  </figure><!--/.story-->
                  <!--MODALS TO SHARE: TWO ACTIONS-->
                    <!--MIXED MODAL WITH CTAS TO REVEAL NETS-->
                    <article class="remodal story-mdl" data-remodal-id="storyToShare-12">
                      <!--POST IMAGE TO SHARE-->
                        <img class="img-storyToShare-12" src="img/crops/mensajes_59.jpg" alt="" />
                      <!--MODAL CTA´s-->
                      <a onclick="ga('send', 'event', 'Sección_Compartir_Mensaje', 'Clic', '/Botón_Compartir_Mensaje_Facebook');" href="javascript:getShareLink('<?php echo base_url() ?>img/crops/mensajes_59.jpg','La gratitud le da sentido al pasado y la paz al presente, compártela')" class="btnModalToShare btn-fbk-ico cta-fbk-mdl">
                        <!--SHARE FaceBook-INTOMODAL-->
                        <strong>Compártelo</strong><span class="iconNet"></span></a>
                      <a onclick="ga('send', 'event', 'Sección_Compartir_Mensaje', 'Clic', '/Botón_Compartir_Mensaje_Twitter');" href="<?php echo base_url('twitter/twitt/i12');?>" class="btnModalToShare btn-twt-ico cta-twt-mdl">
                        <!--SHARE Twitter-INTOMODAL-->
                        <strong>Twitéalo</strong><span class="iconNet"></span></a>
                    </article><!--/.remodal.story-mdl-->
              </li>

              <li>
                  <!--PIN STORY TO ACTIVATE MODAL-->
                  <figure class="story storyToShare-13">
                    <a class="to-reveal-modal" href="#storyToShare-13">
                        <img class="img-storyToShare-13" src="img/crops/mensajes_67.jpg"></a>
                      <figcaption class="ctasOnHov">
                        <div class="boxPrnt">
                          <p class="boxChld">
                            <a href="#storyToShare-13" class="btn-toHov-mdl icoSrch" onclick="ga('send', 'event', 'Sección_Mensajes', 'Clic', '/Botón_Miralo_En_Detalle');">Míralo en detalle</a>
                            <a href="javascript:getShareLink('<?php echo base_url() ?>img/crops/mensajes_67.jpg','Tu energía es lo que te llevará lejos hoy, recarga a tus amigos de buena energía')" class="btn-toHov-mdl icoFbk" onclick="ga('send', 'event', 'Sección_Mensajes', 'Clic', '/Botón_Compartir_Facebook');">Compártelo</a>
                            <a href="<?php echo base_url('twitter/twitt/i13');?>" class="btn-toHov-mdl icoTwt" onclick="ga('send', 'event', 'Sección_Mensajes', 'Clic', '/Botón_Compartir_Twitter');">Twitéalo</a>
                          </p>
                        </div>
                      </figcaption>
                  </figure><!--/.story-->
                  <!--MODALS TO SHARE: TWO ACTIONS-->
                    <!--MIXED MODAL WITH CTAS TO REVEAL NETS-->
                    <article class="remodal story-mdl" data-remodal-id="storyToShare-13">
                      <!--POST IMAGE TO SHARE-->
                        <img class="img-storyToShare-13" src="img/crops/mensajes_67.jpg" alt="" />
                      <!--MODAL CTA´s-->
                      <a onclick="ga('send', 'event', 'Sección_Compartir_Mensaje', 'Clic', '/Botón_Compartir_Mensaje_Facebook');" href="javascript:getShareLink('<?php echo base_url() ?>img/crops/mensajes_67.jpg','Tu energía es lo que te llevará lejos hoy, recarga a tus amigos de buena energía')" class="btnModalToShare btn-fbk-ico cta-fbk-mdl">
                        <!--SHARE FaceBook-INTOMODAL-->
                        <strong>Compártelo</strong><span class="iconNet"></span></a>
                      <a onclick="ga('send', 'event', 'Sección_Compartir_Mensaje', 'Clic', '/Botón_Compartir_Mensaje_Twitter');" href="<?php echo base_url('twitter/twitt/i13');?>" class="btnModalToShare btn-twt-ico cta-twt-mdl">
                        <!--SHARE Twitter-INTOMODAL-->
                        <strong>Twitéalo</strong><span class="iconNet"></span></a>
                    </article><!--/.remodal.story-mdl-->
              </li>

              <li>
                  <!--PIN STORY TO ACTIVATE MODAL-->
                  <figure class="story storyToShare-14">
                    <a class="to-reveal-modal" href="#storyToShare-14">
                        <img class="img-storyToShare-14" src="img/crops/mensajes_69.jpg"></a>
                      <figcaption class="ctasOnHov">
                        <div class="boxPrnt">
                          <p class="boxChld">
                            <a href="#storyToShare-14" class="btn-toHov-mdl icoSrch" onclick="ga('send', 'event', 'Sección_Mensajes', 'Clic', '/Botón_Miralo_En_Detalle');">Míralo en detalle</a>
                            <a href="javascript:getShareLink('<?php echo base_url() ?>img/crops/mensajes_69.jpg','La mejor fortuna es levantarse todos los días con una sonrisa, compártela')" class="btn-toHov-mdl icoFbk" onclick="ga('send', 'event', 'Sección_Mensajes', 'Clic', '/Botón_Compartir_Facebook');">Compártelo</a>
                            <a href="<?php echo base_url('twitter/twitt/i14');?>" class="btn-toHov-mdl icoTwt" onclick="ga('send', 'event', 'Sección_Mensajes', 'Clic', '/Botón_Compartir_Twitter');">Twitéalo</a>
                          </p>
                        </div>
                      </figcaption>
                  </figure><!--/.story-->
                  <!--MODALS TO SHARE: TWO ACTIONS-->
                    <!--MIXED MODAL WITH CTAS TO REVEAL NETS-->
                    <article class="remodal story-mdl" data-remodal-id="storyToShare-14">
                      <!--POST IMAGE TO SHARE-->
                        <img class="img-storyToShare-14" src="img/crops/mensajes_69.jpg" alt="" />
                      <!--MODAL CTA´s-->
                      <a onclick="ga('send', 'event', 'Sección_Compartir_Mensaje', 'Clic', '/Botón_Compartir_Mensaje_Facebook');" href="javascript:getShareLink('<?php echo base_url() ?>img/crops/mensajes_69.jpg','La mejor fortuna es levantarse todos los días con una sonrisa, compártela')" class="btnModalToShare btn-fbk-ico cta-fbk-mdl">
                        <!--SHARE FaceBook-INTOMODAL-->
                        <strong>Compártelo</strong><span class="iconNet"></span></a>
                      <a onclick="ga('send', 'event', 'Sección_Compartir_Mensaje', 'Clic', '/Botón_Compartir_Mensaje_Twitter');" href="<?php echo base_url('twitter/twitt/i14');?>" class="btnModalToShare btn-twt-ico cta-twt-mdl">
                        <!--SHARE Twitter-INTOMODAL-->
                        <strong>Twitéalo</strong><span class="iconNet"></span></a>
                    </article><!--/.remodal.story-mdl-->
              </li>

              <li>
                  <!--PIN STORY TO ACTIVATE MODAL-->
                  <figure class="story storyToShare-15">
                    <a class="to-reveal-modal" href="#storyToShare-15">
                        <img class="img-storyToShare-15" src="img/crops/mensajes_77.jpg"></a>
                      <figcaption class="ctasOnHov">
                        <div class="boxPrnt">
                          <p class="boxChld">
                            <a href="#storyToShare-15" class="btn-toHov-mdl icoSrch" onclick="ga('send', 'event', 'Sección_Mensajes', 'Clic', '/Botón_Miralo_En_Detalle');">Míralo en detalle</a>
                            <a href="javascript:getShareLink('<?php echo base_url() ?>img/crops/mensajes_77.jpg','Sentirse así es una actitud que no puedes dejar para ti, ¡Grítasela al mundo!')" class="btn-toHov-mdl icoFbk" onclick="ga('send', 'event', 'Sección_Mensajes', 'Clic', '/Botón_Compartir_Facebook');">Compártelo</a>
                            <a href="<?php echo base_url('twitter/twitt/i15');?>" class="btn-toHov-mdl icoTwt" onclick="ga('send', 'event', 'Sección_Mensajes', 'Clic', '/Botón_Compartir_Twitter');">Twitéalo</a>
                          </p>
                        </div>
                      </figcaption>
                  </figure><!--/.story-->
                  <!--MODALS TO SHARE: TWO ACTIONS-->
                    <!--MIXED MODAL WITH CTAS TO REVEAL NETS-->
                    <article class="remodal story-mdl" data-remodal-id="storyToShare-15">
                      <!--POST IMAGE TO SHARE-->
                        <img class="img-storyToShare-15" src="img/crops/mensajes_77.jpg" alt="" />
                      <!--MODAL CTA´s-->
                      <a onclick="ga('send', 'event', 'Sección_Compartir_Mensaje', 'Clic', '/Botón_Compartir_Mensaje_Facebook');" href="javascript:getShareLink('<?php echo base_url() ?>img/crops/mensajes_77.jpg','Sentirse así es una actitud que no puedes dejar para ti, ¡Grítasela al mundo!')" class="btnModalToShare btn-fbk-ico cta-fbk-mdl">
                        <!--SHARE FaceBook-INTOMODAL-->
                        <strong>Compártelo</strong><span class="iconNet"></span></a>
                      <a onclick="ga('send', 'event', 'Sección_Compartir_Mensaje', 'Clic', '/Botón_Compartir_Mensaje_Twitter');" href="<?php echo base_url('twitter/twitt/i15');?>" class="btnModalToShare btn-twt-ico cta-twt-mdl">
                        <!--SHARE Twitter-INTOMODAL-->
                        <strong>Twitéalo</strong><span class="iconNet"></span></a>
                    </article><!--/.remodal.story-mdl-->
              </li>
             </ul><!--/#gridHistories-->
             <!--ENDS ALL HISTORIES AND GRID-->
        </section><!--/.deslactosadaGrid.span.twelve-->
      </div><!--/.banner.row-->
    </div><!--/.container-->
  </div>	 