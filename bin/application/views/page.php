<!DOCTYPE html>
<html lang='es'>
  <head>
    <base href="<?php echo base_url() ?>">  
    <!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','//connect.facebook.net/en_US/fbevents.js');

fbq('init', '926731727352142');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=926731727352142&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
    <title>Leche Deslactosada - Alqueria</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="Alqueria, WundermanColombia">
    <link href='//fonts.googleapis.com/css?family=Questrial' rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,700,300,600,400' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/normalize.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/bijou.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery.remodal.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/owl.carousel.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/owl.theme.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/main.css">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!--<link rel="icon" href="favicon.ico">-->
  </head>
  <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-65013244-1', 'auto');
    ga('send', 'pageview');

</script>
  <body class="bodyIntro">
    <!--MAIN HEADER DESLCATOSADA-->
    <header id="mainHead">
      <div class="wrapHeader">
        <div id="mainCow" class="tweenFloat"><img src="img/vaca_home-optmzd.gif">
          <div id="bright"></div></div>
        <hgroup class="titleHead">
          <a href="/"><img src="img/header-title.png" /></a>
          <p><span title="Aquí te mostramos cómo hacerlo">Aquí te mostramos cómo hacerlo</span>
            <strong title="en tres simples pasos">en tres simples pasos</strong></p>
        </hgroup><!--/.wrapHeader-->
        <div id="counter">
          <ul data-count="2146"><!--VALS are CREATED with JS--></ul>
        </div><!--/#counter-->
      </div><!--/.wrapHeader-->
    </header><!--/#mainHead-->
    <!--WELCOME CONTENTS-->
    <div class='container v-wrap'>
      <div class='introBlk v-box'>
        <!--section id="" class="span twelve"></section--><!--/.deslactosadaGrid.span.twelve-->
        <div class='grid'>
          <section id="owl-intro-rules" class='row owl-carousel owl-theme'>
            <figure class='span four ruleNumber item'>
              <img id="rl-Numbr-1" src="img/rules-num-1.png">
              <figcaption>Escoge un mensaje<br/>que quieras compartir<br/>con tus amigos.</figcaption>
            </figure>
            <figure class='span four ruleNumber item'>
              <img id="rl-Numbr-2" src="img/rules-num-2.png">
              <figcaption>Elige a los amigos<br/>que quieres contagiar.</figcaption>
            </figure>
            <figure class='span four ruleNumber item'>
              <img id="rl-Numbr-3" src="img/rules-num-3.png">
              <figcaption><p>Comparte tu mensaje y listo.<br/>¡Ya estás contagiando a los<br/>que más quieres!</p></figcaption>
            </figure>
          </section>
        </div>
        <div class='grid'>
          <div class='row'>
            <a href="<?php echo base_url(); ?>main/index" onclick="ga('send', 'event', 'Home', 'Clic', '/Botón_Comienza_Aquí');" id="mainCTA">
              <img src="img/main-CTA-start-here.png"></a>
          </div>
        </div>
      </div><!--/.banner row-->
    </div><!--/.container-->
    <div class="push"></div>

  <!--FOOTER COPYRIGHTS AND MODAL-->
    <!--MODAL MESSAGE: TERMS & CONDITIONS-->
      <article class="remodal messages-mdl terms-conditions-mdl" data-remodal-id="terms-conditions-message" data-remodal-options="hashTracking: true">
        <header class="header-modal-deslactosada">
          <hgroup><img src="img/header-title.png" /></hgroup>
        </header>
          <h2 class="mdl-subtitles">Términos y condiciones</h2>
          <hr class="terms-hr" />
            <div class="terms-box">
              <h5>¿Quiénes pueden participar?</h5>
              Todas aquellas personas que deseen contagiar a sus amigos de lo bien que se siente mejor aún si lo ha expresado en sus redes sociales facebook y/o Twitter.<br/><br/>
              <h5>¿Cómo participar?</h5>
              Puede hacerlo ingresando directamente al sitio <br/>
              <a href="#">www.sentirsebienescontagioso.com</a> o por la invitación que reciba de la Vaca Madrina en alguno de sus posts positivos dentro de facebook o Twitter. Una vez dentro del sitio sólo debe seguir los siguientes pasos:<br/><br/>
              <h6>1.</h6> Seleccione el mensaje que desea compartir a uno o más amigos. Puede hacerlo por medio de dos alternativas que le ofrece el sitio: imagen o un audio.<br/><br/>
              <h6>2.</h6> Conéctese a cualquiera de las dos redes sociales disponibles (facebook o Twitter) con el usuario y contraseña que usa para acceder a ellas y posteriormente seleccione los amigos a quienes desea compartir el mensaje (en el caso de facebook) o escriba publique el mensaje tagueando a sus amigos en el caso de Twitter (Ej. @sebastian23). Realice la publicación haciendo clic en el botón "Enviar mensaje". Automáticamente quedará publicado en el timeline del usuario un mensaje alusivo al elemento compartido.<br/><br/>
              Por cada amigo al que le comparta un mensaje recibirá un punto. En el caso de Twitterr recibira punto por cada mensaje posteado. Quien acumule el mayor número de puntos recibirá en el domicilio que haya registrado un reconocimiento por parte de Alquería. Sólo podrá compartir dos veces por semana a unmismo amigo. Ser elegirá un usuario reconocido cada semana desde el XX de XXXXX hasta el XX de XXXXX el cual será informado en la fanpage de la actividad.<br/><br/>
              <h6>3.</h6> Sólo cuando se lleve a cabo la primera publicación de mensaje, el sistema le solicitará que ingrese los datos de ubicación para poder hacer envío del reconocimiento en caso que el usuario se haga acreedor a ello.<br/><br/>
              El organizador de la actividad se reserva el derecho se asignación de reconociemintos a usuarios que lleven a cabo actividades fraudulentas o que al menos se sugiera sospecha de ello. La presente actividad busca motivar a los usuarios a contagiar sus sentimientos postivos más allá del mérito competitivo que eso suponga. Por tal motivo el reconomiciento -como su nombre lo indica- es una iniciativa del organizador para agradecer la participación de los usuarios y el cálculo de los puntos acumulados para otorgarlo se hace conforme a lo que el sistema va almacenando de manera privada.
              <br/>
            </div>
          <div class="ctas-mdl-wrap">
            <a href="#" data-remodal-action="cancel" class="cta-btn-mdl-msg btn-red remodal-cancel">Volver</a>
          </div><!--/.ctas-mdl-wrap-->
      </article><!--/.remodal.success-register-mdl-->
    <img src="img/bgMainDeslactosada-internal_4.jpg" style="display:none;">
    <!--FOOT-->
    <footer id="mainFoot">
      <p>Todos los derechos reservados : Alqueria S.A <span class="pipe">|</span> <a onclick="ga('send', 'event', 'Footer', 'Clic', '/Botón_Términos_Y_Condiciones');" href="<?php echo base_url() ?>terminos_y_condiciones_sentirse_bien_es_contagioso.pdf" target="_blank">Términos y condiciones</a></p>
    </footer>

  <!--SCRIPTS-->
  <!--script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script-->
  <!--script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script-->
  <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.js"></script>
  <!--script src="js/jquery-2.1.1.js"></script-->
  <!--script src="js/chromeSmoothScroll.min.js"></script-->
  <script src="<?php echo base_url(); ?>js/jquery.backstretch.min.js"></script>
  <script src="<?php echo base_url(); ?>js/jquery.wookmark.js"></script>
  <script src="<?php echo base_url(); ?>js/jquery.remodal.min.js"></script>
  <script src="<?php echo base_url(); ?>js/owl.carousel.min.js"></script>
  <script src="<?php echo base_url(); ?>js/main.js"></script>
      <!-- Google Code for visitaron Deslactosada - Sentirse bien es contagioso- -->
    <!-- Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. For instructions on adding this tag and more information on the above requirements, read the setup guide: google.com/ads/remarketingsetup -->
    <script type="text/javascript">
    /* <![CDATA[ */
    var google_conversion_id = 982095385;
    var google_conversion_label = "WL2-CJr4rV4Qmaym1AM";
    var google_custom_params = window.google_tag_params;
    var google_remarketing_only = true;
    /* ]]> */
    </script>
    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
    </script>
    <noscript>
    <div style="display:inline;position:absolute;left:0;top:0">
    <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/982095385/?value=1.00&amp;currency_code=COP&amp;label=WL2-CJr4rV4Qmaym1AM&amp;guid=ON&amp;script=0"/>
    </div>
    </noscript>
  </body>
</html>
