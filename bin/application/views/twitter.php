	<div id="app">
		<!--HISTORIES-->
        <section id="deslactosadaGrid" class="span twelve"> 
          <hgroup class="titFltr">
              <h1>Escoge el mensaje con el que quieres contagiar de bienestar a tus amigos,</h1>
              <h2>compártelo y podrás ganar un premio sorpresa.</h2>
            </hgroup>
            <!--NAVIGATION FILTERS-->
            <nav id="filterHeads">
              <ul class="filterCombo">
                <li class="btnFilter"><!--FILTER BY EMOJIS-->
                  <nav class="dropDown">
                    <a class="actvEmoji" href="#d"><span class="emoji emo-1"></span>Selecciona un estado</a>
                    <ul>
                      <li><a href="#" data-filter="emoji-positivo">
                        <span class="emoji emo-1" data-emoji="emo-1"></span>me siento positivo</a></li>
                      <li><a href="#" data-filter="emoji-inspirado">
                        <span class="emoji emo-2" data-emoji="emo-2"></span>me siento inspirado</a></li>
                      <li><a href="#" data-filter="emoji-feliz">
                        <span class="emoji emo-3" data-emoji="emo-3"></span>me siento feliz</a></li>
                      <li><a href="#" data-filter="emoji-alegre">
                        <span class="emoji emo-4" data-emoji="emo-4"></span>me siento alegre</a></li>
                      <li><a href="#" data-filter="emoji-entusiasmado">
                        <span class="emoji emo-5" data-emoji="emo-5"></span>me siento entusiasmado</a></li>
                      <li><a href="#" data-filter="emoji-genial">
                        <span class="emoji emo-6" data-emoji="emo-6"></span>me siento genial</a></li>
                      <li><a href="#" data-filter="emoji-optimista">
                        <span class="emoji emo-7" data-emoji="emo-7"></span>me siento optimista</a></li>
                      <li><a href="#" data-filter="emoji-orgulloso">
                        <span class="emoji emo-8" data-emoji="emo-8"></span>me siento orgulloso</a></li>
                      <li><a href="#" data-filter="emoji-maravillosamente">
                        <span class="emoji emo-9" data-emoji="emo-9"></span>me siento maravillosamente</a></li>
                      <li><a href="#" data-filter="emoji-super">
                        <span class="emoji emo-10" data-emoji="emo-10"></span>me siento súper</a></li>
                      <li><a href="#" data-filter="emoji-motivado">
                        <span class="emoji emo-11" data-emoji="emo-11"></span>me siento motivado</a></li>
                      <li><a href="#" data-filter="emoji-agradecido">
                        <span class="emoji emo-12" data-emoji="emo-12"></span>me siento agradecido</a></li>
                      <li><a href="#" data-filter="emoji-energia">
                        <span class="emoji emo-13" data-emoji="emo-13"></span>me siento lleno de energía</a></li>
                      <li><a href="#" data-filter="emoji-afortunado">
                        <span class="emoji emo-14" data-emoji="emo-14"></span>me siento afortunado</a></li>
                      <li><a href="#" data-filter="emoji-fantastico">
                        <span class="emoji emo-15" data-emoji="emo-15"></span>me siento fantástico</a></li>
                    </ul>
                  </nav>

                </li>
              </ul>
              <ul>
                <li class="btnFilter"><!--FILTER BY AUDIOS-->
                  <a href="#" data-filter="audio">Ver audios<span class="circ inctv"></span></a></li>
              </ul>
              <ul>
                <li class="btnFilter"><!--FILTER BY IMAGES-->
                  <a href="#" data-filter="image">Ver imágen<span class="circ inctv"></span></a></li>
              </ul>
              <ul>
                <li class="btnFilter"><!--VIEW ALL-->
                  <a href="#" data-filter="all">Ver todos<span class="circ inctv"></span></a></li>
              </ul>
              
            </nav><!--/#filterHeads-->
  <ul id="gridHistories">
           		<?php foreach ($content as $item) { ?>
           		<?php if ($item->type == "image") { ?>
	            <li>
	                <figure class="story tipe-<?php echo $item->type?> tipe-emoji-<?php echo $item->emotion . " " . $item->class;?>">
		            	<a class="to-reveal-modal" href="#<?php echo $item->class;?>">
	                        <img class="img-<?php echo $item->class;?>" src="img/crops/<?php echo $item->display_img;?>">
	                    </a>
						<figcaption class="ctasOnHov">
							<div class="boxPrnt">
								<p class="boxChld">
	    	                        <a href="#<?php echo $item->class;?>" class="btn-toHov-mdl icoSrch" onclick="ga('send', 'event', 'Sección_Mensajes', 'Clic', '/Botón_Miralo_En_Detalle');">Míralo en detalle</a>
		                            <a href="javascript:getShareLink('<?php echo $item->share_img; ?>','<?php echo $item->share_msg; ?>')" class="btn-toHov-mdl icoFbk" onclick="ga('send', 'event', 'Sección_Mensajes', 'Clic', '/Botón_Compartir_Facebook');">Compártelo</a>
	                            	<a href="<?php echo base_url('twitter/twitt') .'/'. $item->id;?>" class="btn-toHov-mdl icoTwt" onclick="ga('send', 'event', 'Sección_Mensajes', 'Clic', '/Botón_Compartir_Twitter');">Twitéalo</a>
	                        	</p>
                        </div>
                      </figcaption>
                  </figure>
                   <article class="remodal story-mdl" data-remodal-id="<?php echo $item->class;?>">
                      <!--POST IMAGE TO SHARE-->
                        <img class="img-<?php echo $item->class;?>" src="img/crops/<?php echo $item->display_img;?>" />
                      <!--MODAL CTA´s-->
                      <a onclick="ga('send', 'event', 'Sección_Compartir_Mensaje', 'Clic', '/Botón_Compartir_Mensaje_Facebook');" href="javascript:getShareLink('<?php echo $item->share_img; ?>','<?php echo $item->share_msg; ?>')" class="btnModalToShare btn-fbk-ico cta-fbk-mdl">
                        <!--SHARE FaceBook-INTOMODAL-->
                        <strong>Compártelo</strong><span class="iconNet"></span></a>
                      <a onclick="ga('send', 'event', 'Sección_Compartir_Mensaje', 'Clic', '/Botón_Compartir_Mensaje_Twitter');" href="<?php echo base_url('twitter/twitt') .'/'. $item->id;?>" class="btnModalToShare btn-twt-ico cta-twt-mdl">
                        <!--SHARE Twitter-INTOMODAL-->
                        <strong>Twitéalo</strong><span class="iconNet"></span></a>
                    </article><!--/.remodal.story-mdl-->
              </li>
       			<?php } else if ($item->type == "audio") { ?>
	            <li>
	                <figure class="story tipe-<?php echo $item->type?> tipe-emoji-<?php echo $item->emotion . " " . $item->class;?>">
		            	<a class="to-reveal-modal" href="#<?php echo $item->class;?>">
							<img class="story-pin-img" src="img/crops/audio<?php echo $item->display_img;?>">
						</a>
						<figcaption class="ctasOnHov">
							<div class="boxPrnt">
								<p class="boxChld">
	    	                        <a href="#<?php echo $item->class;?>" class="btn-toHov-mdl icoSrch" onclick="ga('send', 'event', 'Sección_Mensajes', 'Clic', '/Botón_Miralo_En_Detalle');">Míralo en detalle</a>
		                            <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $item->url_fb; ?>" class="btn-toHov-mdl icoFbk" onclick="ga('send', 'event', 'Sección_Mensajes', 'Clic', '/Botón_Compartir_Facebook');">Compártelo</a>
	                            	<a href="<?php echo base_url('twitter/twitt') .'/'. $item->id;?>" class="btn-toHov-mdl icoTwt" onclick="ga('send', 'event', 'Sección_Mensajes', 'Clic', '/Botón_Compartir_Twitter');">Twitéalo</a>
	                        	</p>
							</div>
						</figcaption>
					</figure>
                    <div class="audio-embed-toplay">
                      <div class="avatar-audio">
                        <img src="img/avatar-audio-dem.png"></div>
                      <audio class="mp3embedAudioPlay" src="audios/<?php echo $item->audio_path;?>">  
                        Tu navegador no soporta reproduccion de<code> audio </code> elementos.  
                      </audio>
                    </div>
                  <!--MODALS TO SHARE: PLAY AUDIO THIS POST-->
                    <!-- MODAL WITH CTAS TO SHARE WITH NETS-->
                    <article class="remodal story-mdl" data-remodal-id="<?php echo $item->class;?>">
                      <!--AUDIO EMBED TO SHARE-->
                      <figure class="audio-figure-toShare">
                        <img src="img/histories-twiter-toAudio.jpg">
                        <figcaption>
                          <div class="avatar-audio">
                            <img src="img/avatar-audio-dem.png"></div>
                          <audio class="mp3toAudioPlay" src="audios/<?php echo $item->audio_path;?>">  
                            Tu navegador no soporta reproduccion de<code> audio </code> elementos.  
                          </audio>
                        </figcaption>
                      </figure>
                        <!--img class="img-storyToShare-1" src="img/histories-01Dem-ToPost.jpg" alt="" -->
                      <!--MODAL CTA´s-->
                      <a onclick="ga('send', 'event', 'Sección_Compartir_Mensaje', 'Clic', '/Botón_Compartir_Audio_Facebook');" href="https://www.facebook.com/sharer/sharer.php?u=https://www.facebook.com/100003158365642/videos/vb.100003158365642/810077782440826" class="btnModalToShare btn-fbk-ico cta-fbk-mdl">
                        <!--SHARE FaceBook-INTOMODAL-->
                        <strong>Compártelo</strong><span class="iconNet"></span></a>
                      <a onclick="ga('send', 'event', 'Sección_Compartir_Mensaje', 'Clic', '/Botón_Compartir_Audio_Twitter');" href="<?php echo base_url('twitter/twitt') .'/'. $item->id;?>" class="btnModalToShare btn-twt-ico cta-twt-mdl">
                        <!--SHARE Twitter-INTOMODAL-->
                        <strong>Twitéalo</strong><span class="iconNet"></span></a>
                    </article><!--/.remodal.story-mdl-->
              </li>       			
              <?php } ?>
       		<?php } ?>


             </ul><!--/#gridHistories-->
<?php 
			if($session_id) {  ?>
				<?php if (isset($content_id) && $content_id[0] == "i") { ?>
					 <!--MODAL ACTION: SHARE HISTORIE WITH TWITTER-->
		    <article class="remodal messages-mdl story-mdl-twt" data-remodal-id="storyToShare-withTwt" data-remodal-options="hashTracking: true">
				<form method="POST" enctype="multipart/form-data" id="formTw" action="<?php echo base_url();?>twitter/shareResult" >
		            <header class="header-modal-deslactosada">
		              <hgroup><img src="img/header-title.png" /></hgroup>
		            </header>
		            <p class="intro">Menciona a los amigos que </p>
		            <h2 class="mdl-subtitles">quieres contagiar de bienestar</h2>
		            <div class="the-twit-message">
		            	<input style="display:none;" type="text" name="comment_user" value="<?php echo $msg ?>">
		              <p> <?php echo $msg ?> <span class="hash">#SentirseBienEsContagioso</span> </p>
		            </div>
		            <figure class="img-to-share share-with-twit">
		            	<input style="display:none;" type="text" name="image_url" value="<?php echo $img ?>">
		              <img class="img-storyToShare-1" src="img/post/<?php echo $img ?>" alt="" />
		            </figure>
		            <div class="ctas-mdl-wrap"><!--more-width-->
		              <!--a href="<?php echo base_url();?>twitter/shareResult" class="cta-btn-mdl-msg btn-red">Enviar mensaje</a-->
		              <input type="submit" value="Enviar mensaje" class="cta-btn-mdl-msg btn-red" onclick="ga('send', 'event', 'Sección_Compartir_Redes_Sociales', 'Clic', '/Botón_ Enviar_Mensaje_Twitter');">
		              <!-- <input type="submit" value="Enviar mensaje"> -->
		            </div><!--/.ctas-mdl-wrap-->
		    	</form>				
		    </article><!--/.remodal.messages-mdl.story-mdl-twt-->

		    <?php } else if (isset($content_id) && $content_id[0] == "a") { ?>

		    <article class="remodal messages-mdl story-mdl-twt" data-remodal-id="storyToShare-withTwt" data-remodal-options="hashTracking: true">
				<form method="POST" enctype="multipart/form-data" id="formTw" action="<?php echo base_url();?>twitter/shareTweet" >
		            <header class="header-modal-deslactosada">
		              <hgroup><img src="img/header-title.png" /></hgroup>
		            </header>
		            <p class="intro">Menciona a los amigos que</p>
		            <h2 class="mdl-subtitles">quieres contagiar de bienestar </h2>
		            <div class="the-twit-message">
		            	<input style="display:none;" type="text" name="comment_user" value="Hoy me contagié de bienestar, contágiate tú también. #SentirseBienEsContagioso.<?php echo $url ?>">
		              <p>Hoy me contagié de bienestar, contágiate tú también. <span class="hash">#SentirseBienEsContagioso</span> <?php echo $url ?></p>
		            </div>
		            <figure class="img-to-share share-with-twit">
		              <img class="img-storyToShare-1" src="img/audio/<?php echo $img ?>" alt="" />
		            </figure>
		            <div class="ctas-mdl-wrap"><!--more-width-->
		              <!--a href="<?php echo base_url();?>twitter/shareResult" class="cta-btn-mdl-msg btn-red">Enviar mensaje</a-->
		              <input type="submit" value="Enviar mensaje" class="cta-btn-mdl-msg btn-red" onclick="ga('send', 'event', 'Sección_Compartir_Redes_Sociales', 'Clic', '/Botón_Enviar_Audio_Twitter');">
		              <!-- <input type="submit" value="Enviar mensaje"> -->
		            </div><!--/.ctas-mdl-wrap-->
		    	</form>				
		    </article><!--/.remodal.messages-mdl.story-mdl-twt-->
				<!-- end form-->
				<?php } else { 
			 header('Location:http://www.sentirsebienescontagioso.com/'); 
			} } ?>
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript">
	window.history.pushState({}, '',"#storyToShare-withTwt");
	$(window).load(function() {
		$(".remodal-close").on( "click", function() {
			//console.log("cerrando twitt");
	    	window.location = "http://www.sentirsebienescontagioso.com/main/index";
		});
		$(".remodal-wrapper").on( "click", function() {
			//console.log("cerrando twitt");
	    	window.location = "http://www.sentirsebienescontagioso.com/main/index";
		});
	});
</script>
        	</section><!--/.deslactosadaGrid.span.twelve-->
		</div><!-- Banner Row -->
	</div><!-- Container -->
</div>
