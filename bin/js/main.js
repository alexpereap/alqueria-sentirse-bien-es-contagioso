//PolyFill: https://github.com/entozoon/pointer_events_polyfill
function PointerEventsPolyfill(t){if(this.options={selector:"*",mouseEvents:["click","dblclick","mousedown","mouseup"],usePolyfillIf:function(){if("Microsoft Internet Explorer"==navigator.appName){var t=navigator.userAgent;if(null!=t.match(/MSIE ([0-9]{1,}[\.0-9]{0,})/)){var e=parseFloat(RegExp.$1);if(11>e)return!0}}return!1}},t){var e=this;$.each(t,function(t,n){e.options[t]=n})}this.options.usePolyfillIf()&&this.register_mouse_events()}PointerEventsPolyfill.initialize=function(t){return null==PointerEventsPolyfill.singleton&&(PointerEventsPolyfill.singleton=new PointerEventsPolyfill(t)),PointerEventsPolyfill.singleton},PointerEventsPolyfill.prototype.register_mouse_events=function(){$(document).on(this.options.mouseEvents.join(" "),this.options.selector,function(t){if("none"==$(this).css("pointer-events")||void 0!=$(this).attr("data-pointer-events-none")){var e=$(this).css("display");$(this).css("display","none");var n=document.elementFromPoint(t.clientX,t.clientY);return e?$(this).css("display",e):$(this).css("display",""),t.target=n,$(n).trigger(t),!1}return!0})};

/*READY COMMONS*/
  jQuery(document).ready(function($) { /*"use strict";*/


    //DISPLAY WINNER DIV MESSAGE
    if (jQuery('.winner_flag').first().val() == 1)
    {
      var inst = jQuery('[data-remodal-id=reward-message]').remodal();
      inst.open();      
    }
    else if (jQuery('.user_count_flag').first().val() > 0)
    {
      var inst = jQuery('[data-remodal-id=tryagain-message]').remodal();
      inst.open();      
    }
    else if(jQuery('.user_registered_flag').first().val() == "registered")
    {
      var inst = jQuery('[data-remodal-id=success-register-message]').remodal();
      inst.open();      
    }

    PointerEventsPolyfill.initialize({});
  //HOME
  	if (jQuery('body').hasClass('bodyIntro')) {
      jQuery.backstretch("./img/bgMainDeslactosada-intro.jpg",{fade:200, centeredY: true});
        jQuery('body.bodyIntro').addClass('bgInternalCSS');
        jQuery(document).scroll(function() {
          jQuery(".backstretch img").css('top', '0px');
          jQuery('.bgInternalCSS').css('background-position', 'center bottom');
        });
      function makeRulersMobile(){
        var owl = jQuery("#owl-intro-rules").data('owlCarousel');
        if(jQuery(window).width() < 568 ){
          if (!jQuery('.owl-wrapper-outer')[0]) {  
            jQuery(window).load(function() {
              jQuery("#owl-intro-rules").owlCarousel({
                 navigation : false,
                 slideSpeed : 300,
                 paginationSpeed : 400,
                 singleItem:true
              }); //console.log('create');
            });
          }else{  /* console.log('exists');*/ };
        }else{
          if(jQuery('.owl-wrapper-outer').length > 0) {
            owl.destroy(); //console.log('destroy');
          }else{
            jQuery('section#owl-intro-rules').show().css({ 'opacity':'1', 'display':'block' });
          };
      }//*makeRulersMobile*/
    }
      //CALL ON FIRST LOAD TIME
      makeRulersMobile();
    }else{
      if(jQuery(window).width() > 480 ){
        jQuery.backstretch("./img/bgMainDeslactosada-internal_4.jpg",{fade:200, centeredY: false});
      }else{
        jQuery('body.internal').addClass('bgInternalCSS');
        jQuery.backstretch("./img/bgMainDeslactosada-internal_4.jpg",{fade:200, centeredY: false, centeredX: true});
        jQuery(document).scroll(function() {
          //jQuery('body.internal.bgInternalCSS').css('background-position', 'center ' + (jQuery(document).scrollTop() * 200 / 100 ) + 'px');
          //jQuery('body.internal.bgInternalCSS').css('background-position', 'center ' + (jQuery(document).scrollTop() - 200) + 'px');
          jQuery(".backstretch img").css('top', '0px');
          jQuery('.bgInternalCSS').css('background-position', 'center bottom');
        });
      }

      jQuery(window).scroll(function(e){
        if(jQuery(window).width() > 769 ){
          parallax();
        }
         //showPinHistories(); //INACTIVE BY UPDATE
        cowFollowScrolls();
      });
    };
    //resize call
      jQuery(window).on('resize',function() {
          //console.log('resize');
          //makeRulersMobile();
      }).trigger('resize');
    //FONT TITLE EFFCT
    if(jQuery(window).width() > 769 ){
      jQuery('.titleHead p span, .titleHead p strong, hgroup.titFltr h1, hgroup.titFltr h2').attr('title', function(){
        return jQuery(this).html();
      });
    }

    /*GRIDS HISTORIES*/
  		if (jQuery('#deslactosadaGrid #gridHistories').length > 0){
        preloadHistories();
        jQuery(window).load(function(){
          // Call the layout function.
           // Call the layout function.
          makeHistoriesGrid();
          
        });
        //init
        jQuery('html').scrollTop(0);
          if(jQuery(window).width() > 979 ){
            jQuery('html, body').animate({scrollTop:15}, 'fast'); 
          }

        //AUDIO
        // custom options
        function MakeSetsMP3Player(width){
            var settings = {
                progressbarWidth: width,
                progressbarHeight: '3px',
                progressbarColor: '#1f61ad',
                progressbarBGColor: '#d6d8d9',
                defaultVolume: 0.8
              };
            return settings;
          }
          //**GENERAL CALL AUDIOS*//
          jQuery( ".mp3toAudioPlay" ).each( function( index ){
            jQuery(this).player(MakeSetsMP3Player('225px'));
          });
          jQuery( ".mp3embedAudioPlay" ).each( function( index ){
            jQuery(this).player(MakeSetsMP3Player('150px'));
          });
      };

      /*jQuery('.backstretch').css('background-attachment', 'fixed');
      jQuery(window).scroll(function () {
          document.body.style.backgroundPosition = "0px " + (0 - (Math.max(document.documentElement.scrollTop, document.body.scrollTop) / 4)) + "px";
      });*/
      
      function parallax(){
        var scrolled = jQuery(window).scrollTop();
        jQuery('.backstretch img').css('top',-(scrolled*0.12)+'px');
      };

      var pinsHisFde = jQuery("ul#gridHistories li")/*.fadeTo(0, 0)*/;
      function showPinHistories(){
        //jQuery(window).scroll(function(d,h) {
            pinsHisFde.each(function(i) {
                a = jQuery(this).offset().top + jQuery(this).height();
                b = jQuery(window).scrollTop() + jQuery(window).height();
                if (a < b) jQuery(this).delay(1200).fadeTo(800,1);
            });
        //});
      }
      var sideIsLeft = false;
      function cowFollowScrolls(){
          var $scrollingCow = jQuery("#mainCow.intCow");
            $scrollingCow.addClass('easeTwn');
          //console.log('side is:'+sideIsLeft)
          //jQuery(window).scroll(function(){
          //if (sideIsLeft == false){
            //console.log('isRight');
            //$scrollingCow.stop().removeClass('toSideRight').addClass('toSideLeft').animate({'left': "0%", "marginTop": (jQuery(window).scrollTop() + jQuery(window).height() / 2) + "px"}, 1000/*, 'easeOutSine'*/, function(){ 
                //$scrollingCow.css({'right':'auto'});
              //  sideIsLeft = true;
            //});
          //}else{
            //console.log('isLeft');
            //$scrollingCow.stop().removeClass('toSideLeft').addClass('toSideRight').animate({'left': "80%", "marginTop": (jQuery(window).scrollTop() + jQuery(window).height() / 2) + "px"}, 1000/*, 'easeOutSine'*/, function(){
                //$scrollingCow.css({'left':'auto'});
              //  sideIsLeft = false;
            //});
            
          //}
          //SIMPLE COW ANIMATION
          var theTopMargin;
          if(jQuery(window).width() > 979 ){ theTopMargin = 30;
          }else{ theTopMargin = 0; }
            $scrollingCow.stop().animate({"marginTop": (jQuery(window).scrollTop() + theTopMargin) + "px"}, "slow" );     
          //});
      }
      //cowToModal
        function cowToModal(theModal){
          //console.log('call');
          var theStat = true, $theCow = jQuery('#cowOnModl');
          if ($theCow.length > 0 || $theCow[0] ) {
            //SHOW and REMOVE
            var posTop = /*jQuery(window).scrollTop()*/ - jQuery('#cowOnModl').height(); 
              //console.log('in if');
              jQuery('#cowOnModl').removeClass('cowToModal').animate({"marginTop":  posTop+ "px"}, 500/*, 'easeOutSine'*/, function(){ jQuery('#cowOnModl').remove();
                  jQuery("#mainCow.intCow").stop()/*.delay(1500)*/.animate({ "marginTop": (jQuery(window).scrollTop() + jQuery(window).height() / 2) + "px"}, 600);
              });
            //CONTINUE
          }else{
            //HIDE and CREATE
            var posTopCow = /*jQuery(window).scrollTop()*/ - jQuery('#mainCow.intCow').height();
            jQuery("#mainCow.intCow").animate({ "marginTop": posTopCow + "px"}, 600, function(){ createTwnCowMdl(); });
            function createTwnCowMdl(){
              jQuery("body").append('<div id="cowOnModl"><div id="mdlCow" class="tweenFloat"><div id="shadw"></div><img src="img/vaca_publiaciones.gif"><div id="bright"></div></div></div>');
              var posModal = jQuery(theModal).position(),
                  posLeft = posModal.left - (jQuery('#mdlCow').width() / 2), 
                  posTop = /*jQuery(window).scrollTop()*/ - jQuery('#cowOnModl').height(),
                  //fromTop = (jQuery(window).scrollTop() + ( jQuery(window).height() ) - jQuery('#cowOnModl').height() - 150 );
                  fromTop = (/*jQuery(window).scrollTop() +*/ posModal.top - 65);
                  //console.log('top'+fromTop)
                jQuery('#cowOnModl').css({"marginTop": posTop+'px', "left": posLeft+'px'});
                /*var width = $('#mdlCow').width();
                  var parentWidth = $('#mdlCow').offsetParent().width();
                  var percent = 100*width/parentWidth;*/
                jQuery('#cowOnModl').stop().addClass('cowToModal').animate({"marginTop":  fromTop+ "px"}, 1000);
            } 
            //ENDS
          }
        }; //cowToModal();

      /*COUNTER*/
      //if(jQuery('#counter').length>0){
        function twnCounter(modal){
          var $theModal = jQuery(modal);
          var strCount = jQuery($theModal).find('#counter ul').attr('data-count');
          //console.log(strCount);
          var resNumbs = strCount.split("");
            //jQuery(window).load(function(){
              jQuery(resNumbs).each(function(i, val) {
                setTimeout(function(){ 
                  jQuery($theModal).find("#counter ul").append("<li><p>" + val + "</p></li>");
                  jQuery($theModal).find("#counter ul li").eq(i).find('p').delay(300).fadeIn(300, 
                    function(){ jQuery($theModal).find("#counter ul li").eq(i).addClass('twnn'); 
                  });
                }, i * 150);
              });
            //});
        };
      //};

      /*-MAKE WOOKMARK INIT*/
        function makeHistoriesGrid(){
          jQuery('#gridHistories li:not(.filtered, .simple-player-container ul li)').wookmark({
            autoResize: true, // This will auto-update the layout when the browser window is resized.
            container: jQuery('#gridHistories'), // Optional, used for some extra CSS styling
            offset: 5, // Optional, the distance between grid items
            itemWidth: 286 // Optional, the width of a grid item
          });
        };

      /*PRELOADER*/
        function preloadHistories(){
          
          //jQuery(document).ready(function() {
              //PRELOAD IMAGES
              jQuery("img.lazy").lazy({
                  placeholder: "data:image/gif;base64,R0lGODlhEAALAPQAAP///zl5uuLr9Nrl8e7...",
                  afterLoad: function(element) { jQuery('#gridHistories li').trigger('refreshWookmark'); makeHistoriesGrid(); }
              });
              
          //});
        };

  //SCROLL BAR CUSTOM
      /*jQuery('.contentToScroll').scrollbar({
        "type": "simple"

    //MODAL EVENTS: tween Cow
      jQuery(document).on('opened', '.remodal.messages-mdl', function (e) {
        //console.log('Modal is opened');
         var theModal = e.target;
          cowToModal(theModal);
      });
          /*REWARD TWEEN COUNTER*/
      jQuery(document).on('closed', '.remodal.messages-mdl.reward-mdl', function (e) {
          var inst = jQuery('[data-remodal-id=create-user-action]').remodal();
          inst.open();
          jQuery(e.target).parent().removeClass('remRwrdClass');
      });

      jQuery(document).on('opened', '.remodal.messages-mdl.reward-mdl', function (e) {
          jQuery(e.target).parent().addClass('remRwrdClass');
      });

      jQuery(document).on('opened', '.remodal.messages-mdl.create-user-mdl, .remodal.messages-mdl.reward-mdl', function (e) {
          var theModal = e.target;
          if (!jQuery('#cowOnModl')[0]) {
            cowToModal(theModal);
          };
      });
      

      jQuery(document).on('opened', '.remodal.messages-mdl.reward-mdl, .remodal.messages-mdl.tryagain-mdl', function (e) {
        var theModal = e.target//, theClass = jQuery(theModal).attr('class');
          twnCounter(theModal);
      });

      jQuery(document).on('closed', '.remodal.messages-mdl', function (e) {
          var theModal = e.target;
          cowToModal(theModal);
      });

  //FILTERINGS ADDED
      //*CUSTOM DROPDOWNS*//
        jQuery(".btnFilter .dropDown a.actvEmoji").click(function(event){
            event.preventDefault();
            //event.stopPropagation();
            var theDropdown = jQuery(this).parent().find('ul');
            if( theDropdown.is(':visible') ){ theDropdown.slideUp(300);
            }else{ theDropdown.slideDown(400); }
            theDropdown.parent().find('a.actvEmoji').toggleClass('open');
        });
        //FIGURE BUTTONS ADDED 
          jQuery(".story figcaption.ctasOnHov").click(function(event){
            event.preventDefault();
            event.stopPropagation();
              //jQuery(this).find('.btn-toHov-mdl.icoSrch').trigger('click');
              //console.log('click back');
              //LAUNCH MODAL;
              var id = jQuery(this).find('.btn-toHov-mdl.icoSrch').attr('href'), idClr = id.substring(id.indexOf('#')+1, id.length);
              var inst = jQuery('[data-remodal-id='+idClr+']').remodal();
              inst.open();
          });
          jQuery(".btn-toHov-mdl").click(function(event){
            //event.preventDefault();
              event.stopPropagation();
              //console.log('click btn');
          })
          /*FILTER EMOJIS CONTENT*/
            jQuery('.btnFilter a:not(.actvEmoji)').click(function(event) {
              event.preventDefault();
              //ACTION FILTERS
                if (!jQuery(this).children().hasClass('emoji')) {
                  var defSpan = '<span class="emoji emo-1"></span>Selecciona un estado';
                  jQuery('.dropDown a.actvEmoji').text('').html(defSpan);
                  if( jQuery('.dropDown ul').is(':visible') ){ 
                      jQuery('.dropDown ul').slideUp(300, function(){ }); 
                  }
                  jQuery('a.actvEmoji').removeClass('open');
                }else{ 
                  var theTit = jQuery(this).text(), theClass = jQuery(this).find("span.emoji").attr('data-emoji'), titSpan = '<span class="emoji '+theClass+'"></span>'+theTit;
                    jQuery('.dropDown a.actvEmoji').text('').html(titSpan);
                    jQuery('.dropDown ul').slideUp(300);
                    jQuery('a.actvEmoji').removeClass('open');
                };
                
                // fetch the class of the clicked item
                var theFilter = jQuery(this).attr('data-filter');
                // reset the active class on all the buttons
                /*jQuery(this).parents*/jQuery('.btnFilter').find('a span').addClass('inctv').removeClass('actv');
                // update the active state on our clicked button
                jQuery(this).parent().find('span').removeClass('inctv').addClass('actv');
                //FILTERING
                if(theFilter == 'all') {
                  // show all our items
                  jQuery('#deslactosadaGrid').find('#gridHistories figure.story').parent().show().removeClass('filtered')/*.fadeIn('fast')*/;
                    makeHistoriesGrid();
                }
                else {
                  // hide all elements that don't share theFilter
                  jQuery('#deslactosadaGrid').find('#gridHistories figure:not(.tipe-' + theFilter + ')').parent().hide().addClass('filtered').attr('style', '')/*.fadeOut('fast')*/;
                  // show all elements that do share theFilter
                  jQuery('#deslactosadaGrid').find('#gridHistories figure.tipe-' + theFilter).parent().show().removeClass('filtered')/*.fadeIn('fast')*/;
                    makeHistoriesGrid();
                }
                //return false;
            });

        /*DEFAULT : All Emojis */
          /*data-filter="emoji-positivo
          data-filter="emoji-inspirado
          data-filter="emoji-feliz
          data-filter="emoji-alegre
          data-filter="emoji-entusiasmado
          data-filter="emoji-genial
          data-filter="emoji-optimista
          data-filter="emoji-orgulloso
          data-filter="emoji-maravillosamente
          data-filter="emoji-super
          data-filter="emoji-motivado
          data-filter="emoji-agradecido
          data-filter="emoji-energia">
          data-filter="emoji-afortunado
          data-filter="emoji-fantastico*/


  });
  /*(function($){
    $(window).load(function(){
      
    });
  })(jQuery);*/